package universite.angers.master.info.network.client;

import java.net.Socket;
import org.apache.log4j.Logger;
import universite.angers.master.info.api.translater.Translater;
import universite.angers.master.info.network.communicator.Closeable;
import universite.angers.master.info.network.communicator.Communicator;
import universite.angers.master.info.network.communicator.Openable;
import universite.angers.master.info.network.lock.ReentrantWithReadWriteLock;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.network.socket.SocketService;
import universite.angers.master.info.network.socket.reader.ReaderSocket;
import universite.angers.master.info.network.socket.reader.ReaderSocketStream;
import universite.angers.master.info.network.socket.writer.WriterSocket;
import universite.angers.master.info.network.socket.writer.WriterSocketStream;

/**
 * Client qui permet de communiquer avec un serveur par l'intermédiaire d'un
 * service client
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 * 
 * @param <T> la langue parlé par le client 
 */
public class Client<T> implements Runnable, Openable, Closeable {

	private static final Logger LOG = Logger.getLogger(Client.class);

	/**
	 * Nombre de clients en cours
	 */
	private static int count = 0;

	/**
	 * Host de la socket
	 */
	private String host;
	
	/**
	 * Port de la socket
	 */
	private int port;
	
	/**
	 * La socket permettant de communiquer avec le serveur
	 */
	private Socket clientSocket;

	/**
	 * Le service client qui va communiquer avec le serveur
	 * Il possède toutes les requetes à transmettre au serveur
	 */
	private ClientService<T> clientService;

	/**
	 * Le traducteur entre le service client et socket
	 */
	private Translater<T, String> translatorClientServiceServer;
	
	/**
	 * Le traducteur entre la socket et le service client
	 */
	private Translater<String, T> translatorServerClientService;

	/**
	 * La communication entre la socket et le client
	 */
	private Communicator<T, String> communicatorClientServiceServer;

	/**
	 * Thread qui va contenir le client
	 */
	private Thread threadClient;

	public Client(String host, int port, ClientService<T> clientService,
			Translater<T, String> translatorClientServiceServer, Translater<String, T> translatorServerClientService) {
		try {
			this.host = host;
			LOG.debug("Host : " + host);
			
			this.port = port;
			LOG.debug("Port : " + port);
			
			this.clientSocket = new Socket(host, port);
			LOG.debug("Client socket : " + this.clientSocket);
			
			this.clientService = clientService;
			LOG.debug("Client service : " + this.clientService);
			
			this.translatorClientServiceServer = translatorClientServiceServer;
			LOG.debug("Traducteur client service/serveur : " + this.translatorClientServiceServer);
			
			this.translatorServerClientService = translatorServerClientService;
			LOG.debug("Traducteur serveur/client service: " + this.translatorServerClientService);
			
			this.threadClient = new Thread(this, "Client" + count);
			LOG.debug("Thread client : " + this.threadClient);

			count++;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@Override
	public void run() {
		// On ouvre le lecteur pour lire le contenu de la socket
		ReaderSocket<String> reader = new ReaderSocketStream();
		LOG.debug("Reader socket : " + reader);

		// On ouvre l'écriture de socket pour écrire du contenu dans la socket
		WriterSocket<String> writer = new WriterSocketStream();
		LOG.debug("Writer socket : " + writer);

		// On ouvre le service socket qui permet de lire et écrire
		SocketService<String, T> socketService = new SocketService<>(
				null, null, null, new ReentrantWithReadWriteLock(true),
				this.clientSocket, reader, writer, 
				this.clientService, translatorServerClientService);
		LOG.debug("Service socket : " + socketService);

		// On délégue la communication entre les services : client et socket = serveur
		this.communicatorClientServiceServer = new Communicator<>(this.clientService, socketService,
				this.translatorClientServiceServer);
		LOG.debug("Communication entre client et socket : " + this.communicatorClientServiceServer);

		// On ouvre la communication
		boolean open = this.communicatorClientServiceServer.open();
		LOG.debug("Communication ouverte : " + open);

		// On attend que la communication se termine
		try {
			LOG.debug("Communication en cours ...");
			this.communicatorClientServiceServer.getThreadCommunication().join();
			LOG.debug("Communication terminée");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Notifier le serveur sans retour de réponse
	 * @param subject
	 * @param arg
	 * @return
	 */
	public boolean notifyObserver(String subject, Object arg) {
		LOG.debug("Subject notify : " + subject);

		if(subject == null) return false;
		if(this.communicatorClientServiceServer == null) return false;
		if(this.communicatorClientServiceServer.isClose()) return false;
		
		//On récupère la notification
		if(!this.clientService.getNotifications().containsKey(subject)) return false;
		
		Commandable<T> notification = this.clientService.getNotifications().get(subject);
		LOG.debug("Notification : " + notification);
		
		if(notification == null) return false;
		
		T message = notification.receive(arg);
		LOG.debug("Message notify : " + message);
		
		if(message == null) return false;
		
		//A = Client
		//B = Serveur
		//Le client notifie le serveur
		return this.communicatorClientServiceServer.notifyAtoB(message);
	}
	
	/**
	 * Attendre que toutes les requetes du clients soients traités avant de quitter par exemple
	 */
	public void waitFinish() {
		LOG.debug("Wait requests done");
		while (!this.clientService.isFinish());
		LOG.debug("Requests done");
	}
	
	@Override
	public boolean open() {
		if (this.isOpen())
			return false;

		boolean open = this.clientService.open();
		LOG.debug("Open client service : " + open);
		
		if(open) {
			this.threadClient.start();
			return true;
		}
		else {
			this.clientService.close();
			return false;
		}
	}

	@Override
	public boolean isOpen() {
		return this.threadClient.isAlive() && !this.threadClient.isInterrupted() 
				&& this.communicatorClientServiceServer.isOpen()
				&& !this.clientSocket.isClosed()
				&& this.clientService.isOpen();
	}

	@Override
	public boolean close() {
		if (this.isClose())
			return false;
		
		//On close le service client
		boolean closeClient = this.clientService.close();
		LOG.debug("Service client close : " + closeClient);
		
		// On ferme la communication entre le client et serveur
		boolean closeCom = this.communicatorClientServiceServer.close();
		LOG.debug("Communication fermée : " + closeCom);

		// Puis on ferme le client
		try {
			if (!this.clientSocket.isClosed())
				this.clientSocket.close();
			
			LOG.debug("Client fermée : " + true);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		try {
			//On attend 5s avant d'interrompre le thread
			Thread.sleep(5000);	
			
			//Puis on close le thread
			this.threadClient.interrupt();
			LOG.debug("Thread interrupted : " + this.threadClient.isInterrupted());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return closeClient && closeCom && this.threadClient.isInterrupted();
	}

	@Override
	public boolean isClose() {
		return !this.threadClient.isAlive() || this.threadClient.isInterrupted()
				|| this.communicatorClientServiceServer.isClose()
				|| this.clientSocket.isClosed()
				|| this.clientService.isClose();
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the clientSocket
	 */
	public Socket getClientSocket() {
		return clientSocket;
	}

	/**
	 * @param clientSocket the clientSocket to set
	 */
	public void setClientSocket(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	/**
	 * @return the clientService
	 */
	public ClientService<T> getClientService() {
		return clientService;
	}

	/**
	 * @param clientService the clientService to set
	 */
	public void setClientService(ClientService<T> clientService) {
		this.clientService = clientService;
	}

	/**
	 * @return the translatorClientServiceServer
	 */
	public Translater<T, String> getTranslatorClientServiceServer() {
		return translatorClientServiceServer;
	}

	/**
	 * @param translatorClientServiceServer the translatorClientServiceServer to set
	 */
	public void setTranslatorClientServiceServer(Translater<T, String> translatorClientServiceServer) {
		this.translatorClientServiceServer = translatorClientServiceServer;
	}

	/**
	 * @return the communicatorClientServiceServer
	 */
	public Communicator<T, String> getCommunicatorClientServiceServer() {
		return communicatorClientServiceServer;
	}

	/**
	 * @param communicatorClientServiceServer the communicatorClientServiceServer to set
	 */
	public void setCommunicatorClientServiceServer(Communicator<T, String> communicatorClientServiceServer) {
		this.communicatorClientServiceServer = communicatorClientServiceServer;
	}

	/**
	 * @return the threadClient
	 */
	public Thread getThreadClient() {
		return threadClient;
	}

	/**
	 * @param threadClient the threadClient to set
	 */
	public void setThreadClient(Thread threadClient) {
		this.threadClient = threadClient;
	}

	/**
	 * @return the translatorServerClientService
	 */
	public Translater<String, T> getTranslatorServerClientService() {
		return translatorServerClientService;
	}

	/**
	 * @param translatorServerClientService the translatorServerClientService to set
	 */
	public void setTranslatorServerClientService(Translater<String, T> translatorServerClientService) {
		this.translatorServerClientService = translatorServerClientService;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientService == null) ? 0 : clientService.hashCode());
		result = prime * result + ((clientSocket == null) ? 0 : clientSocket.hashCode());
		result = prime * result
				+ ((communicatorClientServiceServer == null) ? 0 : communicatorClientServiceServer.hashCode());
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + port;
		result = prime * result + ((threadClient == null) ? 0 : threadClient.hashCode());
		result = prime * result
				+ ((translatorClientServiceServer == null) ? 0 : translatorClientServiceServer.hashCode());
		result = prime * result
				+ ((translatorServerClientService == null) ? 0 : translatorServerClientService.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client<?> other = (Client<?>) obj;
		if (clientService == null) {
			if (other.clientService != null)
				return false;
		} else if (!clientService.equals(other.clientService))
			return false;
		if (clientSocket == null) {
			if (other.clientSocket != null)
				return false;
		} else if (!clientSocket.equals(other.clientSocket))
			return false;
		if (communicatorClientServiceServer == null) {
			if (other.communicatorClientServiceServer != null)
				return false;
		} else if (!communicatorClientServiceServer.equals(other.communicatorClientServiceServer))
			return false;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (port != other.port)
			return false;
		if (threadClient == null) {
			if (other.threadClient != null)
				return false;
		} else if (!threadClient.equals(other.threadClient))
			return false;
		if (translatorClientServiceServer == null) {
			if (other.translatorClientServiceServer != null)
				return false;
		} else if (!translatorClientServiceServer.equals(other.translatorClientServiceServer))
			return false;
		if (translatorServerClientService == null) {
			if (other.translatorServerClientService != null)
				return false;
		} else if (!translatorServerClientService.equals(other.translatorServerClientService))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Client [host=" + host + ", port=" + port + ", clientSocket=" + clientSocket + ", clientService="
				+ clientService + ", translatorClientServiceServer=" + translatorClientServiceServer
				+ ", translatorServerClientService=" + translatorServerClientService
				+ ", communicatorClientServiceServer=" + communicatorClientServiceServer + ", threadClient="
				+ threadClient + "]";
	}
}
