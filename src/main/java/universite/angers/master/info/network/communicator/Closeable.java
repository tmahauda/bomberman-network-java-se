package universite.angers.master.info.network.communicator;

/**
 * Interface qui permet de fermer proprement une communication
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Closeable {
	
	/**
	 * Fermer la communication
	 * @return vrai ou faux
	 */
	public boolean close();
	
	/**
	 * Vérifie si la communication est fermée ou non
	 * @return vrai ou faux
	 */
	public boolean isClose();
}
