package universite.angers.master.info.network.communicator;

/**
 * Interface qui permet de communiquer dans une langue T. Elle doit donner la possiblité de :
 * - Ecouter une conversation dans une langue T --> Listenable<T>
 * - Discuter / répondre dans une langue T --> Talkable<T>
 * - Ouvrir une conversation --> Openable
 * - Fermer une conversation --> Closeable
 * - Limiter une conversation --> Limitable
 * - Notifier à tout instant les entités qui communiquent afin de transmettre des messages importants --> Observer
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Communicable<T> extends Listenable<T>, Talkable<T>, Openable, Closeable, Limitable, Observer<T>, Finishable {

}