package universite.angers.master.info.network.communicator;

import org.apache.log4j.Logger;
import universite.angers.master.info.api.translater.Translable;

/**
 * Communication entre deux entités A et B avec l'aide d'un traducteur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 * 
 * @param <T> la langue comprise par l'entité A
 * @param <K> la langue comprise par l'entité B
 */
public class Communicator<T, K> implements Runnable, Openable, Closeable, Limitable, Finishable {

	private static final Logger LOG = Logger.getLogger(Communicator.class);

	/**
	 * Nombre de communication en cours
	 */
	private static int count = 0;
	
	/**
	 * L'entité A qui comprend la langue T
	 */
	private Communicable<T> entityA;

	/**
	 * L'entité B qui comprend la lanque K
	 */
	private Communicable<K> entityB;

	/**
	 * Le traducteur de la langue T vers K et inversement afin que les deux entités
	 * puisse discuter et se comprendre
	 */
	private Translable<T, K> translator;

	/**
	 * Le nombre d'échanges entre l'entité A et B
	 */
	private int request;

	/**
	 * Le thread qui va contenir la communication
	 */
	private Thread threadCommunication;

	public Communicator(Communicable<T> entityA, Communicable<K> entityB, Translable<T, K> translator) {
		this.entityA = entityA;
		LOG.debug("Entité A : " + this.entityA);

		this.entityB = entityB;
		LOG.debug("Entité B : " + this.entityB);

		this.translator = translator;
		LOG.debug("Traducteur : " + this.translator);

		this.request = 0;
		LOG.debug("Echanges : " + this.request);

		this.threadCommunication = new Thread(this, "Communication" + count);
		LOG.debug("Thread communication : " + this.threadCommunication);

		count++;
	}

	/**
	 * Démarrage d'une communication entre deux entités
	 */
	@Override
	public void run() {
		
		/**
		 * Tant que - La communication de l'entité A est actif et non limité par le
		 * nombre d'échanges - La communication de l'entité B est actif et non limité
		 * par le nombre d'échanges - Les deux entités se comprennent
		 */
		while (this.isOpen() && !this.isLimited(this.request)) {

			// Si la communication entre A et B a échouée dans ce cas on stoppe la
			// communication
			boolean understandAAndB = this.communicateBetweenEntityAAndB();
			LOG.debug("A understand B : " + understandAAndB);
			if (!understandAAndB)
				break;
			
			// Si la communication entre B et A a échouée dans ce cas on stoppe la
			// communication
			boolean understandBAndA = this.communicateBetweenEntityBAndA();
			LOG.debug("B understand A : " + understandBAndA);
			if (!understandBAndA)
				break;

			this.request++;
			
			LOG.debug("Echanges : " + this.request);
			LOG.debug("EntityA is open : " + this.entityA.isOpen());
			LOG.debug("EntityA is limited : " + this.entityA.isLimited(this.request));
			LOG.debug("EntityB is open : " + this.entityB.isOpen());
			LOG.debug("EntityB is limited : " + this.entityB.isLimited(this.request));
		}
	}

	/**
	 * Communication entre l'entité A et B
	 * L'entité A parle en premier et B lit le message de A
	 * 
	 * @return vrai si B a compris A. Sinon faux
	 */
	private boolean communicateBetweenEntityAAndB() {	

		// L'entité A parle en premier
		T messageA = this.entityA.talk();
		LOG.debug("Entité A talk : " + messageA);

		// Si le message est vide on stoppe la communication
		if (messageA == null)
			return false;

		// Le message est traduit pour l'entité B
		K messageTranslateToB = this.translator.translateMessageTToK(messageA);
		LOG.debug("Message translate to entity B : " + messageTranslateToB);

		// Si le message traduit est vide on stoppe la communication
		if (messageTranslateToB == null)
			return false;

		// Si l'entité B n'a pas compris le message on stoppe la communication
		boolean entityBUnderstandA = this.entityB.listen(messageTranslateToB);
		LOG.debug("Entity B understand A : " + entityBUnderstandA);

		return entityBUnderstandA;
	}

	/**
	 * Communication entre l'entité B et A
	 * L'entité B parle en premier et A lit le message de B
	 * 
	 * @return vrai si A a compris B. Sinon faux
	 */
	private boolean communicateBetweenEntityBAndA() {
		// L'entité B parle
		K messageB = this.entityB.talk();
		LOG.debug("Entité B talk : " + messageB);

		// Si le message est vide on stoppe la communication
		if (messageB == null)
			return false;

		// Le message est traduit pour l'entité A
		T messageTranslateToA = this.translator.translateMessageKToT(messageB);
		LOG.debug("Message translate to entity B : " + messageTranslateToA);

		// Si le message traduit est vide on stoppe la communication
		if (messageTranslateToA == null)
			return false;

		// Si l'entité A n'a pas compris le message traduit dans ce cas on stoppe la communication
		boolean entityAUnderstandB = this.entityA.listen(messageTranslateToA);
		LOG.debug("Entity A understand B : " + entityAUnderstandB);

		return entityAUnderstandB;
	}

	/**
	 * Méthode qui permet de notifier l'entité A par un message envoyé de B sans
	 * attendre de réponse en retour
	 * @param messageB
	 * @return
	 */
	public boolean notifyBtoA(K messageB) {
		if (messageB == null)
			return false;

		// Le message est traduit pour l'entité A
		T messageTranslateToA = this.translator.translateMessageKToT(messageB);
		LOG.debug("Message translate to entity A : " + messageTranslateToA);

		if (messageTranslateToA == null)
			return false;

		//On passe le message à l'entité A
		return this.entityA.notify(messageTranslateToA);
	}
	
	/**
	 * Méthode qui permet de notifier l'entité B par un message envoyé de A sans
	 * attendre de réponse en retour
	 * @param messageA
	 * @return
	 */
	public boolean notifyAtoB(T messageA) {
		if (messageA == null)
			return false;

		// Le message est traduit pour l'entité B
		K messageTranslateToB = this.translator.translateMessageTToK(messageA);
		LOG.debug("Message translate to entity B : " + messageTranslateToB);

		if (messageTranslateToB == null)
			return false;

		//On passe le message à l'entité A
		return this.entityB.notify(messageTranslateToB);
	}

	@Override
	public boolean finish() {
		boolean entityAFinish = this.entityA.finish();
		boolean entityBFinish = this.entityB.finish();
		return entityAFinish && entityBFinish;
	}
	
	@Override
	public boolean isFinish() {
		return this.entityA.isFinish() || this.entityB.isFinish();
	}

	@Override
	public boolean isLimited(int request) {
		return this.entityA.isLimited(request) || this.entityB.isLimited(request);
	}

	@Override
	public boolean open() {
		if(this.isOpen()) return false;
		
		boolean openA = this.entityA.open();
		boolean openB = this.entityB.open();
		
		if(openA && openB) {
			this.threadCommunication.start();
			return true;
		}
		else return false;
	}

	@Override
	public boolean isOpen() {
		return this.threadCommunication.isAlive() && !this.threadCommunication.isInterrupted() 
				&& this.entityA.isOpen()
				&& this.entityB.isOpen();
	}

	@Override
	public boolean close() {
		if(this.isClose()) return false;
		
		boolean closeEntityA = this.entityA.close();
		boolean closeEntityB = this.entityB.close();
		
		//On attend 10s avant d'interrompre le thread
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			LOG.error(e.getMessage(), e);
		}
		
		this.threadCommunication.interrupt();
		
		return closeEntityA && closeEntityB && this.threadCommunication.isInterrupted();
	}

	@Override
	public boolean isClose() {
		return !this.threadCommunication.isAlive() || this.threadCommunication.isInterrupted()
				|| this.entityA.isClose() 
				|| this.entityB.isClose();
	}

	/**
	 * @return the entityA
	 */
	public Communicable<T> getEntityA() {
		return entityA;
	}

	/**
	 * @param entityA the entityA to set
	 */
	public void setEntityA(Communicable<T> entityA) {
		this.entityA = entityA;
	}

	/**
	 * @return the entityB
	 */
	public Communicable<K> getEntityB() {
		return entityB;
	}

	/**
	 * @param entityB the entityB to set
	 */
	public void setEntityB(Communicable<K> entityB) {
		this.entityB = entityB;
	}

	/**
	 * @return the translator
	 */
	public Translable<T, K> getTranslator() {
		return translator;
	}

	/**
	 * @param translator the translator to set
	 */
	public void setTranslator(Translable<T, K> translator) {
		this.translator = translator;
	}

	/**
	 * @return the request
	 */
	public int getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(int request) {
		this.request = request;
	}

	/**
	 * @return the threadCommunication
	 */
	public Thread getThreadCommunication() {
		return threadCommunication;
	}

	/**
	 * @param threadCommunication the threadCommunication to set
	 */
	public void setThreadCommunication(Thread threadCommunication) {
		this.threadCommunication = threadCommunication;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entityA == null) ? 0 : entityA.hashCode());
		result = prime * result + ((entityB == null) ? 0 : entityB.hashCode());
		result = prime * result + request;
		result = prime * result + ((threadCommunication == null) ? 0 : threadCommunication.hashCode());
		result = prime * result + ((translator == null) ? 0 : translator.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Communicator<?, ?> other = (Communicator<?, ?>) obj;
		if (entityA == null) {
			if (other.entityA != null)
				return false;
		} else if (!entityA.equals(other.entityA))
			return false;
		if (entityB == null) {
			if (other.entityB != null)
				return false;
		} else if (!entityB.equals(other.entityB))
			return false;
		if (request != other.request)
			return false;
		if (threadCommunication == null) {
			if (other.threadCommunication != null)
				return false;
		} else if (!threadCommunication.equals(other.threadCommunication))
			return false;
		if (translator == null) {
			if (other.translator != null)
				return false;
		} else if (!translator.equals(other.translator))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Communicator [entityA=" + entityA + ", entityB=" + entityB + ", translator=" + translator + ", request="
				+ request + ", threadCommunication=" + threadCommunication + "]";
	}
}
