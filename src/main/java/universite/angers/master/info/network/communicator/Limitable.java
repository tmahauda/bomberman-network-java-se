package universite.angers.master.info.network.communicator;

/**
 * Interface qui permet de limiter le nombre de requete entre deux entités
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Limitable {
	
	/**
	 * Vérifie si le quota de requete est limité lors de la communication entre deux entités
	 * @param request
	 * @return
	 */
	public boolean isLimited(int request);
}
