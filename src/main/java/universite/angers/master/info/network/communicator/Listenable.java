package universite.angers.master.info.network.communicator;

/**
 * Interface qui permet d'écouter un message d'une langue T et d'y faire un traitement en fonction du message
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Listenable<T> {
	
	/**
	 * Le traitement à faire en fonction du message
	 * @param response
	 * @return vrai si compris. Faux dans le cas contraire
	 */
	public boolean listen(T response);
}
