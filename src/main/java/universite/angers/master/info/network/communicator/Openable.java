package universite.angers.master.info.network.communicator;

/**
 * Interface qui permet d'ouvrir proprement une communication
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Openable {
	
	/**
	 * Ouvre la communication
	 * @return vrai ou faux
	 */
	public boolean open();
	
	/**
	 * Vérifie si la communication est ouverte ou non
	 * @return
	 */
	public boolean isOpen();
}
