package universite.angers.master.info.network.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Verrou réentrant permet de verrouiller en lecture/écriture avec 
 * distinction entre les deux opérations. On retourne une instance de verrou
 * pour lire et une autre pour écrire
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ReentrantWithReadWriteLock implements ReadWriteLock {

	private ReentrantReadWriteLock lock;
	
	public ReentrantWithReadWriteLock(boolean fair) {
		this.lock = new ReentrantReadWriteLock(fair);
	}
	
	public ReentrantWithReadWriteLock() {
		this.lock = new ReentrantReadWriteLock();
	}
	
	@Override
	public Lock readLock() {
		return this.lock.readLock();
	}

	@Override
	public Lock writeLock() {
		return this.lock.writeLock();
	}
}
