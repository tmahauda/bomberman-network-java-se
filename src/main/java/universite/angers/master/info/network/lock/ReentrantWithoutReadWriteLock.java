package universite.angers.master.info.network.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Verrou réentrant permet de verrouiller en lecture/écriture sans 
 * distinction entre les deux opérations. On retourne la meme instance de verrou
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ReentrantWithoutReadWriteLock implements ReadWriteLock {

	private ReentrantLock lock;
	
	public ReentrantWithoutReadWriteLock(boolean fair) {
		this.lock = new ReentrantLock(fair);
	}
	
	public ReentrantWithoutReadWriteLock() {
		this.lock = new ReentrantLock();
	}
	
	@Override
	public Lock readLock() {
		return this.lock;
	}

	@Override
	public Lock writeLock() {
		return this.lock;
	}
}
