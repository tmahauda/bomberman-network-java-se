package universite.angers.master.info.network.map;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Classe concrete qui implémente BlockingMap<K, V> par une HashMap
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 * 
 * @param <K> la clé
 * @param <V> la valeur
 */
public class BlockingHashMap<K, V> extends BlockingMap<K, V> {

	public BlockingHashMap() {
		super(new ConcurrentHashMap<>());
	}
}
