package universite.angers.master.info.network.map;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import universite.angers.master.info.network.communicator.Closeable;
import universite.angers.master.info.network.communicator.Finishable;
import universite.angers.master.info.network.communicator.Openable;
import universite.angers.master.info.network.communicator.Talkable;

/**
 * Implémentation du Design Pattern "Producer/Consumer" dans une Map.
 * Le consommateur est joué dans la méthode "talk()", là où on récupère des valeurs
 * Le producteur est joué dans les méthodes "put()", là où on ajoute des valeurs
 * En effet Java fournie des listes bloquantes "BlockingQueue" mais pas de Map bloquante !
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 * 
 * @param <K> la clé
 * @param <V> la valeur
 */
public abstract class BlockingMap<K, V> implements ConcurrentMap<K, V>, Talkable<V>, Finishable, Openable, Closeable {

	/**
	 * On ne réinvente pas la roue ! 
	 * On utilise la map concurrente déjà implémenté par Java
	 */
	private ConcurrentMap<K, V> map;
	
	/**
	 * Toutes les valeurs dans la map sont-elles traités dans le talk()
	 */
	private boolean finish;
	
	/**
	 * La map est-elle ouverte ?
	 */
	private boolean open;

	public BlockingMap(ConcurrentMap<K, V> map) {
		this.map = map;
		this.finish = false;
		this.open = false;
	}

	@Override
	public void clear() {
		this.map.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return this.map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return this.map.containsValue(value);
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		return this.map.entrySet();
	}

	@Override
	public V get(Object key) {
		return this.map.get(key);
	}

	@Override
	public boolean isEmpty() {
		return this.map.isEmpty();
	}

	@Override
	public Set<K> keySet() {
		return this.map.keySet();
	}

	@Override
	public synchronized V put(K key, V value) {
		V val = this.map.put(key, value);
		this.notifyAll();
		return val;
	}

	@Override
	public synchronized void putAll(Map<? extends K, ? extends V> m) {
		this.map.putAll(m);
		this.notifyAll();
	}

	@Override
	public synchronized V remove(Object key) {
		V remove = this.map.remove(key);
		this.notifyAll();
		return remove;
	}

	@Override
	public int size() {
		return this.map.size();
	}

	@Override
	public Collection<V> values() {
		return this.map.values();
	}

	@Override
	public synchronized V putIfAbsent(K key, V value) {
		V val = this.map.putIfAbsent(key, value);
		this.notifyAll();
		return val;
	}

	@Override
	public synchronized boolean remove(Object key, Object value) {
		boolean remove = this.map.remove(key, value);
		this.notifyAll();
		return remove;
	}

	@Override
	public synchronized V replace(K key, V value) {
		V val = this.map.replace(key, value);
		this.notifyAll();
		return val;
	}

	@Override
	public synchronized boolean replace(K key, V oldValue, V newValue) {
		boolean replace = this.map.replace(key, oldValue, newValue);
		this.notifyAll();
		return replace;
	}

	@Override
	public synchronized V talk() {
		while (this.open && this.map.isEmpty()) {
			try {
				//Il n'y a plus de requete a traité. Donc c'est terminé
				this.finish = true;
				//Le thread courant attend. Le verrou est relaché !
				this.wait();
			} catch (Exception e) {
				return null;
			}
		}

		//Il y a une requete a traité. Donc ce n'est pas terminé
		this.finish = false;
		
		//On prend la première valeur trouvé
		Optional<K> optionalKey = this.map.keySet().stream().findFirst();
		if(!optionalKey.isPresent()) return null;
		
		K key = optionalKey.get();
		V val = this.map.get(key);
		
		//On supprime la valeur
		this.map.remove(key);
		
		return val;
	}
	
	@Override
	public synchronized boolean finish() {
		this.map.clear();
		return this.map.isEmpty();
	}

	@Override
	public synchronized boolean isFinish() {
		return this.finish;
	}

	@Override
	public synchronized boolean close() {
		this.open = false;
		this.notifyAll();
		return true;
	}

	@Override
	public synchronized boolean isClose() {
		return !this.open;
	}

	@Override
	public synchronized boolean open() {
		this.open = true;
		this.notifyAll();
		return true;
	}

	@Override
	public synchronized boolean isOpen() {
		return this.open;
	}

	/**
	 * @param finish the finish to set
	 */
	public synchronized void setFinish(boolean finish) {
		this.finish = finish;
	}

	/**
	 * @param open the open to set
	 */
	public synchronized void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (finish ? 1231 : 1237);
		result = prime * result + ((map == null) ? 0 : map.hashCode());
		result = prime * result + (open ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlockingMap<?, ?> other = (BlockingMap<?, ?>) obj;
		if (finish != other.finish)
			return false;
		if (map == null) {
			if (other.map != null)
				return false;
		} else if (!map.equals(other.map))
			return false;
		if (open != other.open)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BlockingMap [map=" + map + ", finish=" + finish + ", open=" + open + "]";
	}
}
