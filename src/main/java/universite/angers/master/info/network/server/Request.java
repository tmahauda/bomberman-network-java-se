package universite.angers.master.info.network.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import org.apache.log4j.Logger;
import universite.angers.master.info.api.Accessiable;
import universite.angers.master.info.api.Charset;
import universite.angers.master.info.api.HttpStatusCode;
import universite.angers.master.info.api.MimeType;
import universite.angers.master.info.network.util.NetworkUtil;

public class Request implements Accessiable<String, String> {

	private static final Logger LOG = Logger.getLogger(Request.class);
	
	/**
	 * L'URL pour accéder à l'API
	 */
	private String url;
	
	/**
	 * L'encodage du message
	 */
	protected Charset characterEncoding;
	
	/**
	 * Le contenu type du message (XML, JSON, ...)
	 */
	protected MimeType contentType;
	
	/**
	 * Le résultat de la réponse
	 */
	protected HttpStatusCode result;
	
	public Request(String url, Charset characterEncoding, MimeType contentType) {
		this.url = url;
		this.characterEncoding = characterEncoding;
		this.contentType = contentType;
	}

	private boolean executeWrite(String method, String object) {
		try {
			URL url = new URL (this.url);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setRequestMethod(method);
			con.setRequestProperty("Content-Type", this.contentType.getName() + "; " + this.characterEncoding.getName());
			con.setRequestProperty("Accept", this.contentType.getName());
			con.setDoOutput(true);
			con.setDoInput(true);
			
			OutputStream os = con.getOutputStream();
			os.write(object.getBytes(this.characterEncoding.getName()));
			os.flush();
			os.close();

			int codeResult = con.getResponseCode();
			this.result = HttpStatusCode.getStatus(codeResult);
			
			return this.result == HttpStatusCode.OK;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}
	
	@Override
	public synchronized boolean create(String object) {
		boolean create = this.executeWrite("POST", object);
		LOG.debug("Create with method post : " + create);
		
		return create;
	}

	@Override
	public synchronized boolean delete(String object) {
		boolean delete = this.executeWrite("DELETE", object);
		LOG.debug("Delete with method delete : " + delete);
		
		return delete;
	}
	
	@Override
	public synchronized boolean update(String object) {
		boolean update = this.executeWrite("PUT", object);
		LOG.debug("Update with method put : " + update);
		
		return update;
	}

	private String executeRead(String attribut, String value) {
		try {
			String path = "";
			if(NetworkUtil.isNullOrEmpty(attribut) || NetworkUtil.isNullOrEmpty(value)) {
				path = this.url;
			} else {
				path = this.url+"/"+attribut+"/"+value;
			}
			
			URL url = new URL (path);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Content-Type", this.contentType.getName() + "; " + this.characterEncoding.getName());
			con.setRequestProperty("Accept", this.contentType.getName());
			con.setDoOutput(true);
			con.setDoInput(true);
			
			StringBuilder content = new StringBuilder();

            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            	
                String line;

                while ((line = in.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }
            }

			int codeResult = con.getResponseCode();
			this.result = HttpStatusCode.getStatus(codeResult);
			
			return content.toString();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return "";
		}
	}
	
	@Override
	public synchronized String read(String attribut, Object value) {
		String read = this.executeRead(attribut, value.toString());
		LOG.debug("Read with condition with method get : " + read);
		
		return read;
	}
	
	@Override
	public synchronized String read(Map<String, Object> where) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public synchronized String readAll() {
		String readAll = this.executeRead(null, null);
		LOG.debug("Read all without condition with method get : " + readAll);
		
		return readAll;
	}
	
	
	@Override
	public synchronized String readAll(Map<String, Object> where) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public synchronized String readAll(String attribut, Object value) {
		String read = this.executeRead(attribut, value.toString());
		LOG.debug("Read all with condition with method get : " + read);
		
		return read;
	}
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the characterEncoding
	 */
	public Charset getCharacterEncoding() {
		return characterEncoding;
	}

	/**
	 * @param characterEncoding the characterEncoding to set
	 */
	public void setCharacterEncoding(Charset characterEncoding) {
		this.characterEncoding = characterEncoding;
	}

	/**
	 * @return the contentType
	 */
	public MimeType getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(MimeType contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the result
	 */
	public HttpStatusCode getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(HttpStatusCode result) {
		this.result = result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((characterEncoding == null) ? 0 : characterEncoding.hashCode());
		result = prime * result + ((contentType == null) ? 0 : contentType.hashCode());
		result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Request other = (Request) obj;
		if (characterEncoding != other.characterEncoding)
			return false;
		if (contentType != other.contentType)
			return false;
		if (result != other.result)
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Request [url=" + url + ", characterEncoding=" + characterEncoding + ", contentType=" + contentType
				+ ", result=" + result + "]";
	}
}
