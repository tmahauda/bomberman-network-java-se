package universite.angers.master.info.network.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.apache.log4j.Logger;
import universite.angers.master.info.api.translater.Translater;
import universite.angers.master.info.network.communicator.Closeable;
import universite.angers.master.info.network.communicator.Communicator;
import universite.angers.master.info.network.communicator.Openable;
import universite.angers.master.info.network.lock.ReentrantWithReadWriteLock;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.network.socket.SocketService;
import universite.angers.master.info.network.socket.reader.ReaderSocket;
import universite.angers.master.info.network.socket.reader.ReaderSocketStream;
import universite.angers.master.info.network.socket.writer.WriterSocket;
import universite.angers.master.info.network.socket.writer.WriterSocketStream;

/**
 * Serveur qui permet de communiquer avec des clients par l'intermédiaire d'un
 * service serveur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 * 
 * @param <T> la langue parlé par le serveur
 */
public class Server<T> implements Runnable, Openable, Closeable, Observable<String, T> {

	private static final Logger LOG = Logger.getLogger(Server.class);

	/**
	 * Nombre de server en cours
	 */
	private static int count = 0;
	
	/**
	 * Host de la socket
	 */
	private String host;
	
	/**
	 * Port de la socket
	 */
	private int port;

	/**
	 * Le nombre maximum que peut traiter le serveur
	 */
	private int requestMax;

	/**
	 * Le server socket permettant de communiquer avec le ou les clients
	 */
	private ServerSocket serverSocket;

	/**
	 * Le service serveur qui va communiquer avec les clients
	 */
	private ServerService<T> serverService;

	/**
	 * Le traducteur entre les clients et le serveur service
	 */
	private Translater<T, String> translatorServiceClientsServer;
	
	/**
	 * Le traducteur entre le service serveur et les clients
	 */
	private Translater<String, T> translatorServerServiceClients;

	/**
	 * La communication entre le service serveur et les clients
	 */
	private ConcurrentMap<String, Communicator<String, T>> communicatorServerServiceClients;

	/**
	 * Thread qui va contenir le serveur
	 */
	private Thread threadServer;
	
	/**
	 * Thread qui va vérifier les communications actifs en envoyant des echos request toutes les minutes
	 */
	private Thread threadEchosRequests;

	/**
	 * Nombre de connexions de client
	 */
	private int request;

	public Server(String host, int port, int requestMax, ServerService<T> serverService,
			Translater<T, String> translatorServiceClientsServer, Translater<String, T> translatorServerServiceClients) {
		try {
			this.host = host;
			LOG.debug("Host : " + this.host);
			
			this.port = port;
			LOG.debug("Port : " + this.port);
			
			this.requestMax = requestMax;
			LOG.debug("Request max : " + this.requestMax);
			
			this.serverSocket = new ServerSocket(port, requestMax, InetAddress.getByName(host));
			LOG.debug("Server socket : " + this.serverSocket);
			
			this.serverService = serverService;
			LOG.debug("Server service : " + this.serverService);
			
			this.translatorServiceClientsServer = translatorServiceClientsServer;
			LOG.debug("Traducteur service/client serveur : " + this.translatorServerServiceClients);
			
			this.translatorServerServiceClients = translatorServerServiceClients;
			LOG.debug("Traducteur serveur service/client : " + this.translatorServerServiceClients);
			
			this.communicatorServerServiceClients = new ConcurrentHashMap<>();
			LOG.debug("Communications serveur/clients : " + this.communicatorServerServiceClients);
			
			this.threadServer = new Thread(this, "Server"+count);
			LOG.debug("Thread server : " + this.threadServer);
			
			this.threadEchosRequests = this.getThreadEchosRequests();
			
			count++;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	/**
	 * Thread qui permet d'envoyer toutes les minutes un echo request afin de vérifier les communications actifs
	 * des clients
	 * @return
	 */
	private Thread getThreadEchosRequests() {
		return new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(isOpen()) {
					//On attend 1 minute avant chaque echo request
					try {
						Thread.sleep(60000);
						
						//On notifie tous les clients d'un echo pour vérifier qu'ils sont vivants
						//notifyAllObservers(Subject.NOTIFY_ECHO.getName(), null);
						
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
					}
				}
				
			}
		}, "EchoRequest");
	}
	
	@Override
	public void run() {
		// Tant que le serveur est ouvert
		while (this.isOpen()) {

			// On ouvre le lecteur de socket
			ReaderSocket<String> reader = new ReaderSocketStream();
			LOG.debug("Reader socket : " + reader);

			// On ouvre l'écriture de socket
			WriterSocket<String> writer = new WriterSocketStream();
			LOG.debug("Writer socket : " + writer);

			Socket clientSocket = null;
			try {
				clientSocket = this.serverSocket.accept();
				LOG.debug("Client socket : " + clientSocket);
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
				break;
			}

			// On ouvre le service socket
			SocketService<String, T> socketService = new SocketService<>(
					null, null, null, new ReentrantWithReadWriteLock(true),
					clientSocket, reader, writer, 
					this.serverService, this.translatorServerServiceClients);
			LOG.debug("Service socket : " + socketService);

			// On délégue la communication entre les services
			Communicator<String, T> communicatorServerServiceClient = new Communicator<>(socketService,
					this.serverService, this.translatorServerServiceClients);
			LOG.debug("Communication entre socket et serveur: " + communicatorServerServiceClient);

			// On ouvre la communication
			boolean open = communicatorServerServiceClient.open();
			LOG.debug("Communication ouverte : " + open);

			if(open) {
//				//On souhaite la bienvenue au client
//				boolean helloClient = this.notifyObserver(Subject.NOTIFY_HELLO.getName(), communicatorServerServiceClient, clientSocket);
//				LOG.debug("Notify hello client : " + helloClient);
//				
//				//On notifie les autres clients d'un nouveau client
//				boolean newClient = this.notifyAllObservers(Subject.NOTIFY_NEW.getName(), clientSocket);
//				LOG.debug("Notify new client : " + newClient);
				
				String port = String.valueOf(clientSocket.getPort());
				LOG.debug("Port socket : " + port);
				
				String ip = clientSocket.getInetAddress().toString();
				LOG.debug("IP socket : " + ip);
				
				String id = port+ip;
				LOG.debug("ID socket : " + id);
				
				// On ajoute la communication entre le serveur et le client dans la map
				this.communicatorServerServiceClients.put(
						id,
						communicatorServerServiceClient);
				LOG.debug("Ajout de la communication dans la map");
			}

			this.request++;
		}
	}

	@Override
	public boolean notifyObserver(String subject, Communicator<String, T> com, Object arg) {
		LOG.debug("Subject notify : " + subject);
		LOG.debug("Communication : " + com);
		
		if(subject == null) return false;
		if(com == null) return false;
		if(com.isClose()) return false;
		
		//On récupère la notification
		if(!this.serverService.getNotifications().containsKey(subject)) return false;
		
		Commandable<T> notification = this.serverService.getNotifications().get(subject);
		LOG.debug("Notification : " + notification);
		
		if(notification == null) return false;
		
		T message = null;
		if(arg == null) {
			message = notification.receive(com);
		} else {
			message = notification.receive(arg);
		}
		
		LOG.debug("Message notify : " + message);
		
		if(message == null) return false;
		
		//B = Serveur
		//A = Client
		//Le serveur notifie le client
		return com.notifyBtoA(message);
	}
	/**
	 * Fonction qui permet de restreindre la communication 
	 */
	public boolean notifyAllObservers(Collection<Communicator<String, T>> comsToRestreint, String subject, Object arg) {
		Iterator<Entry<String, Communicator<String, T>>> it = this.communicatorServerServiceClients.entrySet().iterator();
		
		while(it.hasNext()) {
		
			Entry<String, Communicator<String, T>> com = it.next();
			
			if(comsToRestreint.contains(com.getValue())) continue;
				
			boolean notify = this.notifyObserver(subject, com.getValue(), arg);
			LOG.debug("Notify com : " + notify);
			
			//Si la notification n'a pas pu se faire pour une com dans ce cas suppression dans la liste
			if(!notify) {
				com.getValue().close();
				it.remove();
			}
		}
		
		return true;
	}
	@Override
	public boolean notifyAllObservers(String subject, Object arg) {
		
		Iterator<Entry<String, Communicator<String, T>>> it = this.communicatorServerServiceClients.entrySet().iterator();
		
		while(it.hasNext()) {
			
			Entry<String, Communicator<String, T>> com = it.next();
			
			boolean notify = this.notifyObserver(subject, com.getValue(), arg);
			LOG.debug("Notify com : " + notify);
			
			//Si la notification n'a pas pu se faire pour une com dans ce cas suppression dans la liste
			if(!notify) {
				com.getValue().close();
				it.remove();
			}
		}
		
		return true;
	}
	
	@Override
	public boolean open() {
		if (this.isOpen())
			return false;

		boolean open = this.serverService.open();
		LOG.debug("Open server service : " + open);
		
		if(open) {
			this.threadServer.start();
			this.threadEchosRequests.start();
			return true;
		} 
		else {
			this.serverService.close();
			return false;
		}
	}

	@Override
	public boolean isOpen() {
		return this.threadServer.isAlive() && !this.threadServer.isInterrupted() && 
				!this.serverSocket.isClosed() &&
				this.serverService.isOpen();
	}

	@Override
	public boolean close() {
		if (this.isClose())
			return false;

		// On close le service serveur
		boolean closeServerService = this.serverService.close();
		LOG.debug("Service serveur fermée : " + closeServerService);
		
		//On close toutes les communications avec les clients
		for (Iterator<Entry<String, Communicator<String, T>>> it = this.communicatorServerServiceClients.entrySet()
				.iterator(); it.hasNext();) {
			Entry<String, Communicator<String, T>> com = it.next();
			
			boolean closeCom = com.getValue().close();
			LOG.debug("Communication client fermée : " + closeCom);
			
			it.remove();
		}

		this.communicatorServerServiceClients.clear();
		
		// Enfin on close le serveur
		try {
			this.serverSocket.close();
			
			//On attend 5s avant d'interrompre le thread echos
			Thread.sleep(5000);
			
			this.threadEchosRequests.interrupt();
			
			//On attend 5s avant d'interrompre le thread server
			Thread.sleep(5000);
			
			this.threadServer.interrupt();
			
			return true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public boolean isClose() {
		return !this.threadServer.isAlive() || this.threadServer.isInterrupted() || 
				this.serverSocket.isClosed() ||
				this.serverService.isClose();
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the requestMax
	 */
	public int getRequestMax() {
		return requestMax;
	}

	/**
	 * @param requestMax the requestMax to set
	 */
	public void setRequestMax(int requestMax) {
		this.requestMax = requestMax;
	}

	/**
	 * @return the serverSocket
	 */
	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	/**
	 * @param serverSocket the serverSocket to set
	 */
	public void setServerSocket(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}

	/**
	 * @return the serverService
	 */
	public ServerService<T> getServerService() {
		return serverService;
	}

	/**
	 * @param serverService the serverService to set
	 */
	public void setServerService(ServerService<T> serverService) {
		this.serverService = serverService;
	}

	/**
	 * @return the translatorServerServiceClients
	 */
	public Translater<String, T> getTranslatorServerServiceClients() {
		return translatorServerServiceClients;
	}

	/**
	 * @param translatorServerServiceClients the translatorServerServiceClients to set
	 */
	public void setTranslatorServerServiceClients(Translater<String, T> translatorServerServiceClients) {
		this.translatorServerServiceClients = translatorServerServiceClients;
	}

	/**
	 * @return the communicatorServerServiceClients
	 */
	public ConcurrentMap<String, Communicator<String, T>> getCommunicatorServerServiceClients() {
		return communicatorServerServiceClients;
	}

	/**
	 * @param communicatorServerServiceClients the communicatorServerServiceClients to set
	 */
	public void setCommunicatorServerServiceClients(
			ConcurrentMap<String, Communicator<String, T>> communicatorServerServiceClients) {
		this.communicatorServerServiceClients = communicatorServerServiceClients;
	}

	/**
	 * @return the threadServer
	 */
	public Thread getThreadServer() {
		return threadServer;
	}

	/**
	 * @param threadServer the threadServer to set
	 */
	public void setThreadServer(Thread threadServer) {
		this.threadServer = threadServer;
	}

	/**
	 * @return the request
	 */
	public int getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(int request) {
		this.request = request;
	}
	
	/**
	 * @return the translatorServiceClientsServer
	 */
	public Translater<T, String> getTranslatorServiceClientsServer() {
		return translatorServiceClientsServer;
	}

	/**
	 * @param translatorServiceClientsServer the translatorServiceClientsServer to set
	 */
	public void setTranslatorServiceClientsServer(Translater<T, String> translatorServiceClientsServer) {
		this.translatorServiceClientsServer = translatorServiceClientsServer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((communicatorServerServiceClients == null) ? 0 : communicatorServerServiceClients.hashCode());
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + port;
		result = prime * result + request;
		result = prime * result + requestMax;
		result = prime * result + ((serverService == null) ? 0 : serverService.hashCode());
		result = prime * result + ((serverSocket == null) ? 0 : serverSocket.hashCode());
		result = prime * result + ((threadServer == null) ? 0 : threadServer.hashCode());
		result = prime * result
				+ ((translatorServerServiceClients == null) ? 0 : translatorServerServiceClients.hashCode());
		result = prime * result
				+ ((translatorServiceClientsServer == null) ? 0 : translatorServiceClientsServer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Server<?> other = (Server<?>) obj;
		if (communicatorServerServiceClients == null) {
			if (other.communicatorServerServiceClients != null)
				return false;
		} else if (!communicatorServerServiceClients.equals(other.communicatorServerServiceClients))
			return false;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (port != other.port)
			return false;
		if (request != other.request)
			return false;
		if (requestMax != other.requestMax)
			return false;
		if (serverService == null) {
			if (other.serverService != null)
				return false;
		} else if (!serverService.equals(other.serverService))
			return false;
		if (serverSocket == null) {
			if (other.serverSocket != null)
				return false;
		} else if (!serverSocket.equals(other.serverSocket))
			return false;
		if (threadServer == null) {
			if (other.threadServer != null)
				return false;
		} else if (!threadServer.equals(other.threadServer))
			return false;
		if (translatorServerServiceClients == null) {
			if (other.translatorServerServiceClients != null)
				return false;
		} else if (!translatorServerServiceClients.equals(other.translatorServerServiceClients))
			return false;
		if (translatorServiceClientsServer == null) {
			if (other.translatorServiceClientsServer != null)
				return false;
		} else if (!translatorServiceClientsServer.equals(other.translatorServiceClientsServer))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Server [host=" + host + ", port=" + port + ", requestMax=" + requestMax + ", serverSocket="
				+ serverSocket + ", serverService=" + serverService + ", translatorServiceClientsServer="
				+ translatorServiceClientsServer + ", translatorServerServiceClients=" + translatorServerServiceClients
				+ ", communicatorServerServiceClients=" + communicatorServerServiceClients + ", threadServer="
				+ threadServer + ", request=" + request + "]";
	}
}