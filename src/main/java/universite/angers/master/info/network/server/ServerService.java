package universite.angers.master.info.network.server;

import java.util.concurrent.locks.ReadWriteLock;
import org.apache.log4j.Logger;
import universite.angers.master.info.api.converter.Convertable;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.network.service.Service;

/**
 * Classe qui permet de communiquer avec le client à travers des commandes
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerService<T> extends Service<T> {

	private static final Logger LOG = Logger.getLogger(ServerService.class);
	
	public ServerService(Commandable<T> requestDefault, Commandable<T> notificationDefault, 
			Convertable<String, T> commandName, ReadWriteLock lock) {
		super(requestDefault, notificationDefault, commandName, lock);
	}

	@Override
	public T talk() {
		
		if(this.requestCurrent == null) {
			T talk = this.requestDefault.receive(null);
			this.lock.writeLock().unlock();
			return talk;
		}
			
		//On émet la réponse à la requete
		T message = this.requestCurrent.receive(null);
		this.requestCurrent = null;
		
		this.lock.writeLock().unlock();
		
		return message;
	}
	
	@Override
	public boolean listen(T response) {
		LOG.debug("Response : " + response);
		
		if(response == null) this.requestDefault.send(response);
		
		this.lock.writeLock().lock();
		
		String commandName = this.commandName.convert(response);
		LOG.debug("Command name : " + commandName);
		
		if(commandName == null) {
			boolean send = this.requestDefault.send(response);
			this.lock.writeLock().unlock();
			return send;
		}
		
		if(!this.requests.containsKey(commandName)) {
			boolean send = this.requestDefault.send(response);
			this.lock.writeLock().unlock();
			return send;
		}
		
		Commandable<T> command = this.requests.get(commandName);
		LOG.debug("Command : " + command);
		
		if(command == null) {
			boolean send = this.requestDefault.send(response);
			this.lock.writeLock().unlock();
			return send;
		}
		
		this.requestCurrent = command;
		
		boolean listen = command.send(response);
		
		if(!listen) {
			this.lock.writeLock().unlock();
			return false;
		} else return true;
	}

	@Override
	public boolean close() {
		return false;
	}

	@Override
	public boolean isClose() {
		return false;
	}

	@Override
	public boolean isLimited(int request) {
		return false;
	}

	@Override
	public boolean finish() {
		return true;
	}

	@Override
	public boolean isFinish() {
		return true;
	}
}
