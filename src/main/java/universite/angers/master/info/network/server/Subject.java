package universite.angers.master.info.network.server;

public enum Subject {
	
	NOTIFY_ECHO("NOTIFY_ECHO"),
	NOTIFY_HELLO("NOTIFY_HELLO"),
	NOTIFY_BYE("NOTIFY_BYE"),
	NOTIFY_NEW("NOTIFY_NEW"),
	NOTIFY_CLOSE("NOTIFY_CLOSE"),
	NOTIFY_DEFAULT("NOTIFY_DEFAULT");
	
	private String name;
	
	private Subject(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
