package universite.angers.master.info.network.service;

/**
 * Interface qui permet d'envoyer ou recevoir une commande d'une requete
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Commandable<T> {

	/**
	 * Recevoir la réponse de la commande
	 * @param message
	 * @return
	 */
	public boolean send(T message);
	
	/**
	 * Envoyer une commande avec éventuellement des arguments
	 * @return
	 */
	public T receive(Object arg);
}
