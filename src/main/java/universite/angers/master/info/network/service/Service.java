package universite.angers.master.info.network.service;

import java.util.concurrent.locks.ReadWriteLock;
import org.apache.log4j.Logger;
import universite.angers.master.info.api.converter.Convertable;
import universite.angers.master.info.network.communicator.Communicable;
import universite.angers.master.info.network.map.BlockingHashMap;
import universite.angers.master.info.network.map.BlockingMap;
import universite.angers.master.info.network.util.NetworkUtil;

/**
 * Service qui permet de traiter des requetes et des notifications
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 * 
 * @param <T> la langue parlé par le service
 */
public abstract class Service<T> implements Communicable<T> {

	private static final Logger LOG = Logger.getLogger(Service.class);

	/**
	 * Liste des requetes à traiter
	 * Key = nom de la requete
	 * Value = la requete à traiter
	 */
	protected BlockingMap<String, Commandable<T>> requests;
	
	/**
	 * La requete par défaut si la réponse est inconnu afin d'éviter de casser
	 * la communication si non comprise
	 */
	protected Commandable<T> requestDefault;
	
	/**
	 * La requete courante qui est traité
	 */
	protected Commandable<T> requestCurrent;
	
	/**
	 * Liste des notifications
	 * Key = nom de la notification
	 * Value = la notification à traiter
	 */
	protected BlockingMap<String, Commandable<T>> notifications;
	
	/**
	 * La commande par défaut si une notification est inconnu afin d'éviter de casser
	 * la communication si non comprise
	 */
	protected Commandable<T> notificationDefault;
	
	/**
	 * Récupérer le nom de la commande dans une notification ou requete dans la réponse
	 */
	protected Convertable<String, T> commandName;
	
	/**
	 * Verrou qui permet de synchroniser les commandes
	 */
	protected ReadWriteLock lock;
	
	public Service(Commandable<T> requestDefault, Commandable<T> notificationDefault, 
			Convertable<String, T> commandName, ReadWriteLock lock) {
		this.requests = new BlockingHashMap<>();
		LOG.debug("Requests " + this.requests);
		
		this.notifications = new BlockingHashMap<>();
		LOG.debug("Notifications " + this.notifications);
		
		this.requestDefault = requestDefault;
		LOG.debug("Requete par défaut : " + this.requestDefault);
		
		this.notificationDefault = notificationDefault;
		LOG.debug("Notification par défaut : " + this.notificationDefault);
		
		this.commandName = commandName;
		LOG.debug("Command name " + this.commandName);
		
		this.lock = lock;
		LOG.debug("Verrou " + this.lock);
	}
	

	@Override
	public boolean isNotify(T message) {
		LOG.debug("Message notify : " + message);
		if(message == null) return true;
		
		// On récupère le nom de la commande dans le message
		String commandName = this.commandName.convert(message);
		LOG.debug("Command name notify : " + commandName);
		
		//Si la commande est vide par défaut c'est une notification qui sera traité à part
		if(NetworkUtil.isNullOrEmpty(commandName)) return true;
		
		//C'est une notification si non contenu dans la liste des requetes
		boolean notify = this.notifications.containsKey(commandName);
		LOG.debug("Is notify : " + notify);
		
		return notify;
	}
	
	@Override
	public boolean notify(T message) {
		LOG.debug("Message notify : " + message);
		if(message == null) return this.notificationDefault.send(message);
		
		String commandName = this.commandName.convert(message);
		LOG.debug("Command name notify : " + commandName);
		
		//Si la commande est vide par défaut par défaut on renvoi vrai pour notification recu
		if(NetworkUtil.isNullOrEmpty(commandName)) return this.notificationDefault.send(message);
		if(!this.notifications.containsKey(commandName)) return this.notificationDefault.send(message);
		
		Commandable<T> command = this.notifications.get(commandName);
		LOG.debug("Command : " + command);
		
		if(command == null) return this.notificationDefault.send(message);
		
		return command.send(message);
	}
	
	/**
	 * Enregistrer une requete
	 * @param request
	 * @param command
	 * @return
	 */
	public boolean registerRequest(String request, Commandable<T> command) {
		LOG.debug("Request : " + request);
		LOG.debug("Command : " + command);
		
		if(NetworkUtil.isNullOrEmpty(request)) return false;
		if(command == null) return false;
		
		if(!this.requests.containsKey(request)) {
			this.requests.put(request, command);
			LOG.debug("Requete ajoutée");
			return true;
		} else {
			LOG.debug("Requete non ajoutée");
			return false;
		}
	}
	
	/**
	 * Enregistre une notification
	 * @param notification
	 * @param command
	 * @return
	 */
	public boolean registerNotification(String notification, Commandable<T> command) {
		LOG.debug("Notification : " + notification);
		LOG.debug("Command : " + command);
		
		if(NetworkUtil.isNullOrEmpty(notification)) return false;
		if(command == null) return false;
		
		if(!this.notifications.containsKey(notification)) {
			this.notifications.put(notification, command);
			LOG.debug("Notification ajoutée");
			return true;
		} else {
			LOG.debug("Notification non ajoutée");
			return false;
		}
	}
	

	@Override
	public boolean open() {
		return this.requests.open() && this.notifications.open();
	}

	@Override
	public boolean isOpen() {
		return this.requests.isOpen() && this.notifications.isOpen();
	}

	/**
	 * @return the requests
	 */
	public BlockingMap<String, Commandable<T>> getRequests() {
		return requests;
	}

	/**
	 * @param requests the requests to set
	 */
	public void setRequests(BlockingMap<String, Commandable<T>> requests) {
		this.requests = requests;
	}

	/**
	 * @return the requestDefault
	 */
	public Commandable<T> getRequestDefault() {
		return requestDefault;
	}

	/**
	 * @param requestDefault the requestDefault to set
	 */
	public void setRequestDefault(Commandable<T> requestDefault) {
		this.requestDefault = requestDefault;
	}

	/**
	 * @return the requestCurrent
	 */
	public Commandable<T> getRequestCurrent() {
		return requestCurrent;
	}

	/**
	 * @param requestCurrent the requestCurrent to set
	 */
	public void setRequestCurrent(Commandable<T> requestCurrent) {
		this.requestCurrent = requestCurrent;
	}

	/**
	 * @return the notifications
	 */
	public BlockingMap<String, Commandable<T>> getNotifications() {
		return notifications;
	}

	/**
	 * @param notifications the notifications to set
	 */
	public void setNotifications(BlockingMap<String, Commandable<T>> notifications) {
		this.notifications = notifications;
	}

	/**
	 * @return the notificationDefault
	 */
	public Commandable<T> getNotificationDefault() {
		return notificationDefault;
	}

	/**
	 * @param notificationDefault the notificationDefault to set
	 */
	public void setNotificationDefault(Commandable<T> notificationDefault) {
		this.notificationDefault = notificationDefault;
	}

	/**
	 * @return the commandName
	 */
	public Convertable<String, T> getCommandName() {
		return commandName;
	}

	/**
	 * @param commandName the commandName to set
	 */
	public void setCommandName(Convertable<String, T> commandName) {
		this.commandName = commandName;
	}

	/**
	 * @return the lock
	 */
	public ReadWriteLock getLock() {
		return lock;
	}

	/**
	 * @param lock the lock to set
	 */
	public void setLock(ReadWriteLock lock) {
		this.lock = lock;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commandName == null) ? 0 : commandName.hashCode());
		result = prime * result + ((lock == null) ? 0 : lock.hashCode());
		result = prime * result + ((notificationDefault == null) ? 0 : notificationDefault.hashCode());
		result = prime * result + ((notifications == null) ? 0 : notifications.hashCode());
		result = prime * result + ((requestCurrent == null) ? 0 : requestCurrent.hashCode());
		result = prime * result + ((requestDefault == null) ? 0 : requestDefault.hashCode());
		result = prime * result + ((requests == null) ? 0 : requests.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service<?> other = (Service<?>) obj;
		if (commandName == null) {
			if (other.commandName != null)
				return false;
		} else if (!commandName.equals(other.commandName))
			return false;
		if (lock == null) {
			if (other.lock != null)
				return false;
		} else if (!lock.equals(other.lock))
			return false;
		if (notificationDefault == null) {
			if (other.notificationDefault != null)
				return false;
		} else if (!notificationDefault.equals(other.notificationDefault))
			return false;
		if (notifications == null) {
			if (other.notifications != null)
				return false;
		} else if (!notifications.equals(other.notifications))
			return false;
		if (requestCurrent == null) {
			if (other.requestCurrent != null)
				return false;
		} else if (!requestCurrent.equals(other.requestCurrent))
			return false;
		if (requestDefault == null) {
			if (other.requestDefault != null)
				return false;
		} else if (!requestDefault.equals(other.requestDefault))
			return false;
		if (requests == null) {
			if (other.requests != null)
				return false;
		} else if (!requests.equals(other.requests))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Service [requests=" + requests + ", requestDefault=" + requestDefault + ", requestCurrent="
				+ requestCurrent + ", notifications=" + notifications + ", notificationDefault=" + notificationDefault
				+ ", commandName=" + commandName + ", lock=" + lock + "]";
	}
}
