package universite.angers.master.info.network.socket;

import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.ReadWriteLock;

import org.apache.log4j.Logger;

import universite.angers.master.info.api.converter.Convertable;
import universite.angers.master.info.api.translater.Translable;
import universite.angers.master.info.network.communicator.Communicable;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.network.service.Service;
import universite.angers.master.info.network.socket.reader.ReaderSocket;
import universite.angers.master.info.network.socket.writer.WriterSocket;

/**
 * Classe qui de lire et écrire dans une socket lors de la communication avec cette entité
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class SocketService<T, K> extends Service<T> implements Runnable {

	private static final Logger LOG = Logger.getLogger(SocketService.class);
	
	/**
	 * Flag qui permet de lire un message jusqu'à rencontrer un "END"
	 */
	public static final String END = "END";
	
	/**
	 * La socket
	 */
	private Socket socket;
	
	/**
	 * Le lecteur qui permet de lire une socket
	 */
	private ReaderSocket<T> reader;
	
	/**
	 * L'écriture dans une socket
	 */
	private WriterSocket<T> writer;
	
	/**
	 * Thread qui va "écouter" continuellement les messages dans le "read"
	 */
	private Thread threadListen;
	
	/**
	 * Les messages des requetes à traiter dans le talk()
	 */
	private BlockingQueue<T> messages;
	
	/**
	 * Le traducteur entre la socket qui comprend la langue T et l'entité 
	 * qui comprend la langue K avec qui il discute
	 */
	private Translable<T, K> translator;
	
	/**
	 * L'entité qui discute en langue K avec la socket
	 */
	private Communicable<K> entity;
	
	public SocketService(
			Commandable<T> requestDefault, Commandable<T> notificationDefault, 
			Convertable<String, T> commandName, ReadWriteLock lock,
			Socket socket, ReaderSocket<T> reader, WriterSocket<T> writer, 
			Communicable<K> entity, Translable<T, K> translator) {
		super(requestDefault, notificationDefault, commandName, lock);
		this.socket = socket;
		this.reader = reader;
		this.writer = writer;
		this.reader.setSocket(this.socket);
		this.writer.setSocket(this.socket);
		this.messages = new ArrayBlockingQueue<>(1);
		this.threadListen = new Thread(this);
		this.entity = entity;
		this.translator = translator;
	}
	
	@Override
	public void run() {
		while(this.isOpen()) {

			T message = this.reader.read();
			
			LOG.debug("Message notify : " + message);
			
			if (message == null)
				break;

			K messageTranslate = this.translator.translateMessageTToK(message);
			LOG.debug("Message translate notify : " + messageTranslate);

			if (messageTranslate == null)
				break;

			//Si c'est une notification on passe le message à l'autre entité
			//Sinon on traite le message dans le talk()
			if(this.entity.isNotify(messageTranslate)) {
				LOG.debug("Message is notify");
				this.entity.notify(messageTranslate);	
			} else {
				LOG.debug("Message is not a notify");
				this.messages.add(message);
			}
		}
	}
	
	@Override
	public boolean isNotify(T message) {
		return false;
	}
	
	@Override
	public boolean notify(T message) {
		//On pose un verrou
		this.lock.writeLock().lock();
		
		//On écrit le message
		boolean listen = this.writer.write(message);
		
		//On attend 1ms le temps que le message soit bien envoyé et correctement lu de l'autre coté
		//avant de relacher le verrou
		try {
			Thread.sleep(100);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		//On relache le verrou
		this.lock.writeLock().unlock();
		
		return listen;
	}
	
	@Override
	public T talk() {
		try {
			//L'appel est bloquant. La méthode retourne le message
			//si le thread qui écoute ajoute un message
			return this.messages.take();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}
	
	@Override
	public boolean listen(T response) {
		return this.notify(response);
	}
	
	@Override
	public boolean isLimited(int request) {
		return false;
	}
	
	@Override
	public boolean finish() {
		return true;
	}
	
	@Override
	public boolean isFinish() {
		return true;
	}
	
	@Override
	public boolean open() {
		boolean openReader = this.reader.open();
		boolean openWriter = this.writer.open();
		
		if(openReader && openWriter) {
			this.threadListen.start();	
			return true;
		} else {
			this.reader.close();
			this.writer.close();
			return false;
		}
	}

	@Override
	public boolean isOpen() {
		return this.reader.isOpen() && this.writer.isOpen() && 
				!this.socket.isClosed() && this.socket.isConnected();
	}
	
	@Override
	public boolean close() {
		try {
			boolean readerClosed = this.reader.close();
			boolean writerClosed = this.writer.close();
			this.socket.close();
			
			//On attend 5s avant d'interrompre le thread
			Thread.sleep(5000);
			
			this.threadListen.interrupt();
			
			return readerClosed && writerClosed && this.socket.isClosed() && this.threadListen.isInterrupted();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public boolean isClose() {
		return this.reader.isClose() || this.writer.isClose() || 
				this.socket.isClosed() || !this.socket.isConnected();
	}
	
	/**
	 * @return the socket
	 */
	public Socket getSocket() {
		return socket;
	}

	/**
	 * @param socket the socket to set
	 */
	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	/**
	 * @return the reader
	 */
	public ReaderSocket<T> getReader() {
		return reader;
	}

	/**
	 * @param reader the reader to set
	 */
	public void setReader(ReaderSocket<T> reader) {
		this.reader = reader;
	}

	/**
	 * @return the writer
	 */
	public WriterSocket<T> getWriter() {
		return writer;
	}

	/**
	 * @param writer the writer to set
	 */
	public void setWriter(WriterSocket<T> writer) {
		this.writer = writer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((reader == null) ? 0 : reader.hashCode());
		result = prime * result + ((socket == null) ? 0 : socket.hashCode());
		result = prime * result + ((writer == null) ? 0 : writer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SocketService<?, ?> other = (SocketService<?, ?>) obj;
		if (reader == null) {
			if (other.reader != null)
				return false;
		} else if (!reader.equals(other.reader))
			return false;
		if (socket == null) {
			if (other.socket != null)
				return false;
		} else if (!socket.equals(other.socket))
			return false;
		if (writer == null) {
			if (other.writer != null)
				return false;
		} else if (!writer.equals(other.writer))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CommunicatorSocket [socket=" + socket + ", reader=" + reader + ", writer=" + writer + "]";
	}
}
