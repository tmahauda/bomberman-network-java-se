package universite.angers.master.info.network.socket.reader;

/**
 * Interface qui permet de lire un message d'une langue T
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Readable<T> {
	
	/**
	 * Lire le contenu
	 * @return
	 */
	public T read();
}
