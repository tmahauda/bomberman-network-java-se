package universite.angers.master.info.network.socket.reader;

import java.net.Socket;
import org.apache.log4j.Logger;

import universite.angers.master.info.network.communicator.Closeable;
import universite.angers.master.info.network.communicator.Openable;

/**
 * Classe qui permet de lire un message T dans une socket
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class ReaderSocket<T> implements Readable<T>, Openable, Closeable {

	private static final Logger LOG = Logger.getLogger(ReaderSocket.class);
	
	protected Socket socket;
	
	public ReaderSocket() {
		this(null);
	}
	
	public ReaderSocket(Socket socket) {
		this.socket = socket;
		LOG.debug("Socket : " + socket);
	}
	
	@Override
	public boolean open() {
		return this.socket != null;
	}

	@Override
	public boolean isOpen() {
		return !this.socket.isInputShutdown();
	}

	@Override
	public boolean close() {
		try {
			if(!this.socket.isInputShutdown())
				this.socket.shutdownInput();
			
			return this.socket.isInputShutdown();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public boolean isClose() {
		return this.socket.isInputShutdown();
	}
	
	/**
	 * @return the socket
	 */
	public Socket getSocket() {
		return socket;
	}

	/**
	 * @param socket the socket to set
	 */
	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((socket == null) ? 0 : socket.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReaderSocket<?> other = (ReaderSocket<?>) obj;
		if (socket == null) {
			if (other.socket != null)
				return false;
		} else if (!socket.equals(other.socket))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ReaderSocket [socket=" + socket + "]";
	}
}
