package universite.angers.master.info.network.socket.reader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import org.apache.log4j.Logger;

import universite.angers.master.info.network.socket.SocketService;

/**
 * Classe qui permet de lire le message en format String d'une socket avec l'aide d'un inputStream
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ReaderSocketStream extends ReaderSocket<String> {

	private static final Logger LOG = Logger.getLogger(ReaderSocketStream.class);
	
	public ReaderSocketStream() {
		super();
	}

	public ReaderSocketStream(Socket socket) {
		super(socket);
	}

	@Override
	public String read() {
		LOG.debug("Socket : " + this.socket);

		try {
			//Si le lecteur est fermé
			if(this.socket.isInputShutdown()) return null;
			
			InputStream is = this.socket.getInputStream();
			InputStreamReader in = new InputStreamReader(is, StandardCharsets.UTF_8);
			BufferedReader br = new BufferedReader(in);
			
			StringBuilder sb = new StringBuilder();
			
			//Tant qu'il y a des lignes à lire
			while (true) {
				
				//Attention ! readLine est bloquant
				String line = br.readLine();
				LOG.debug("Line : " + line);
				
				//On quitte la boucle si le lecteur est fermé ou il y a un "END"
				if (line == null || line.equals(SocketService.END)) break;
				
				sb.append(line);
				sb.append(System.lineSeparator());
			}
		    
		    return sb.toString();
		 
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ReaderSocketStream [socket=" + socket + "]";
	}
}
