package universite.angers.master.info.network.socket.writer;

/**
 * Interface qui permet d'ecrire un message d'une langue T
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Writable<T> {
	
	/**
	 * Ecrire un message T
	 * @param message
	 * @return
	 */
	public boolean write(T message);
}
