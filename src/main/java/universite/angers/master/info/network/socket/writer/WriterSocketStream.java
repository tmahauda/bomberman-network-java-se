package universite.angers.master.info.network.socket.writer;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import org.apache.log4j.Logger;

import universite.angers.master.info.network.socket.SocketService;

/**
 * Classe qui permet d'écrire le message en format String d'une socket avec l'aide d'un outputStream
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class WriterSocketStream extends WriterSocket<String> {

	private static final Logger LOG = Logger.getLogger(WriterSocketStream.class);
	
	public WriterSocketStream() {
		super();
	}
	
	public WriterSocketStream(Socket socket) {
		super(socket);
	}

	@Override
	public boolean write(String message) {
		LOG.debug("Socket : " + this.socket);
		LOG.debug("Message to write : " + message);
		
		try {
			//Si le lecteur est fermé
			if(this.socket.isOutputShutdown()) return false;
			
			OutputStream os = this.socket.getOutputStream();
			OutputStreamWriter out = new OutputStreamWriter(os, StandardCharsets.UTF_8);
			BufferedWriter bw = new BufferedWriter(out);
			PrintWriter writer = new PrintWriter(bw, true);
			
			//On ajoute END a la fin du message pour quitter le read dans le while
			message += "\n" + SocketService.END;
			
            writer.println(message);
            writer.flush();
            
            LOG.debug("Write OK");
            
            return true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WriterSocketStream [socket=" + socket + "]";
	}
}
