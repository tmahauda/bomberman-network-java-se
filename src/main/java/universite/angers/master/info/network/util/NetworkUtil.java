package universite.angers.master.info.network.util;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Classe utilitaire de network
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class NetworkUtil {

	/**
	 * Constructeur afin d'empecher l'instanciation de la classe
	 */
	private NetworkUtil() {
		
	}
	
	/**
	 * Vérifier si une chaine est vide
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmpty(String str) {
        if(str != null && !str.trim().isEmpty())
            return false;
        return true;
    }
}
