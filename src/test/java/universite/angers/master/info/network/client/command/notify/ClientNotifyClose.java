package universite.angers.master.info.network.client.command.notify;

import java.net.Socket;

import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.network.service.Commandable;

public class ClientNotifyClose implements Commandable<String> {

	@Override
	public boolean send(String message) {
		System.out.println(message);
		return true;
	}

	@Override
	public String receive(Object arg) {
		Socket client = (Socket)arg;
		
		//On donne le port local afin de le supprimer coté serveur
		//En effet la clé de la map du serveur est le port de la socket
		return Subject.NOTIFY_CLOSE.getName()+";"+client.getLocalPort();
	}
}
