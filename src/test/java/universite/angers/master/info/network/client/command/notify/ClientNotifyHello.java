package universite.angers.master.info.network.client.command.notify;

import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.network.service.Commandable;

public class ClientNotifyHello implements Commandable<String> {

	@Override
	public boolean send(String message) {
		System.out.println(message);
		return true;
	}

	@Override
	public String receive(Object arg) {
		return Subject.NOTIFY_HELLO.getName()+";Hello";
	}
}
