package universite.angers.master.info.network.client.command.request;

import universite.angers.master.info.network.client.command.DateObject;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de demander la date du jour au serveur et
 * d'enregistrer la réponse recu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientRequestDay implements Commandable<String> {

	private DateObject date;
	
	public ClientRequestDay(DateObject date) {
		this.date = date;
	}
	
	@Override
	public boolean send(String message) {
		this.date.setDay(message);
		return true;
	}

	@Override
	public String receive(Object arg) {
		return "DAY";
	}
}
