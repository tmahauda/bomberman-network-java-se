package universite.angers.master.info.network.client.command.request;

import universite.angers.master.info.network.client.command.DateObject;
import universite.angers.master.info.network.service.Commandable;

/**
 * Classe qui permet de demander l'année du jour au serveur et
 * d'enregistrer la réponse recu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientRequestYear implements Commandable<String> {

	private DateObject date;
	
	public ClientRequestYear(DateObject date) {
		this.date = date;
	}
	
	@Override
	public boolean send(String message) {
		this.date.setYear(message);
		return true;
	}

	@Override
	public String receive(Object arg) {
		return "YEAR";
	}
}
