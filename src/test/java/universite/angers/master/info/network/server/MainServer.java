package universite.angers.master.info.network.server;

import java.util.Collection;
import java.util.Date;
import org.apache.log4j.Logger;
import universite.angers.master.info.api.translater.TranslaterStringToString;
import universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock;
import universite.angers.master.info.network.server.api.UserAPI;
import universite.angers.master.info.network.server.command.ConverterCommandNameServer;
import universite.angers.master.info.network.server.command.DateUtil;
import universite.angers.master.info.network.server.command.notify.ServerNotifyClose;
import universite.angers.master.info.network.server.command.notify.ServerNotifyEcho;
import universite.angers.master.info.network.server.command.notify.ServerNotifyHello;
import universite.angers.master.info.network.server.command.notify.ServerNotifyNew;
import universite.angers.master.info.network.server.command.request.ServerRequestDay;
import universite.angers.master.info.network.server.command.request.ServerRequestHour;
import universite.angers.master.info.network.server.command.request.ServerRequestMonth;
import universite.angers.master.info.network.server.command.request.ServerRequestYear;
import universite.angers.master.info.network.server.model.User;

/**
 * Classe qui permet de lancer le serveur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class MainServer {

	private static final Logger LOG = Logger.getLogger(MainServer.class);
	
	public static void main(String[] args) {
	    String host = "127.0.0.1";
	    int port = 2345;
	    
	    DateUtil date = new DateUtil();
	    
	    ServerService<String> service = new ServerService<>(null, null, new ConverterCommandNameServer(), new ReentrantWithoutReadWriteLock(true));
	    Server<String> server = new Server<>(host, port, 10, service, new TranslaterStringToString(), new TranslaterStringToString());
	    
	    //Les requetes
	    service.registerRequest("DAY", new ServerRequestDay(date));
		service.registerRequest("MONTH", new ServerRequestMonth(date));
		service.registerRequest("YEAR", new ServerRequestYear(date));
		service.registerRequest("TIME", new ServerRequestHour(date));
		
		//Les notifications
		service.registerNotification(Subject.NOTIFY_ECHO.getName(), new ServerNotifyEcho());
		service.registerNotification(Subject.NOTIFY_HELLO.getName(), new ServerNotifyHello());
		service.registerNotification(Subject.NOTIFY_NEW.getName(), new ServerNotifyNew());
		service.registerNotification(Subject.NOTIFY_CLOSE.getName(), new ServerNotifyClose());
	    
	    //Ici on a qu'un serveur mais on pourrait imaginer lancer
	    //plusieurs serveur simultanément
	    server.open();
	    
	    //testAPIUser();
	}
	
	private static void testAPIUser() {
	    User user = new User("Test", "TEST", "test@gmail.com", "test", "test",
				"France", new Date());
	    
	    //Création d'un user dans le JEE
		UserAPI.getInstance().create(user);
		displayUsers();
		
		user.setCountry("Maroc");
		UserAPI.getInstance().update(user);
		displayUsers();
		
		UserAPI.getInstance().delete(user);
		displayUsers();
		
		User toto = UserAPI.getInstance().read("pseudo", "Toto");
		LOG.debug(toto);
	}
	
	private static void displayUsers() {
		Collection<User> users = UserAPI.getInstance().readAll();
		
		for(User user : users) {
			LOG.debug(user);
		}
	}
}
	
	/** Résultat obtenu : OK et cohérent
	 23:01:12,623 DEBUG main:Service:<init>:69 - Requests BlockingMap [map={}, finish=false, open=false]
23:01:12,624 DEBUG main:Service:<init>:72 - Notifications BlockingMap [map={}, finish=false, open=false]
23:01:12,624 DEBUG main:Service:<init>:75 - Requete par défaut : null
23:01:12,624 DEBUG main:Service:<init>:78 - Notification par défaut : null
23:01:12,626 DEBUG main:Service:<init>:81 - Command name universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454
23:01:12,626 DEBUG main:Service:<init>:84 - Verrou universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf
23:01:12,633 DEBUG main:Server:<init>:92 - Host : 127.0.0.1
23:01:12,633 DEBUG main:Server:<init>:95 - Port : 2345
23:01:12,633 DEBUG main:Server:<init>:98 - Request max : 10
23:01:12,636 DEBUG main:Server:<init>:101 - Server socket : ServerSocket[addr=/127.0.0.1,localport=2345]
23:01:12,636 DEBUG main:Server:<init>:104 - Server service : Service [requests=BlockingMap [map={}, finish=false, open=false], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={}, finish=false, open=false], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf]
23:01:12,636 DEBUG main:Server:<init>:107 - Traducteur serveur service/client : universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2
23:01:12,636 DEBUG main:Server:<init>:110 - Communications serveur/clients : {}
23:01:12,636 DEBUG main:Server:<init>:113 - Thread server : Thread[Server0,5,main]
23:01:12,637 DEBUG main:Service:registerRequest:134 - Request : DAY
23:01:12,637 DEBUG main:Service:registerRequest:135 - Command : universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5
23:01:12,639 DEBUG main:Service:registerRequest:142 - Requete ajoutée
23:01:12,639 DEBUG main:Service:registerRequest:134 - Request : MONTH
23:01:12,639 DEBUG main:Service:registerRequest:135 - Command : universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d
23:01:12,639 DEBUG main:Service:registerRequest:142 - Requete ajoutée
23:01:12,640 DEBUG main:Service:registerRequest:134 - Request : YEAR
23:01:12,640 DEBUG main:Service:registerRequest:135 - Command : universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2
23:01:12,640 DEBUG main:Service:registerRequest:142 - Requete ajoutée
23:01:12,641 DEBUG main:Service:registerRequest:134 - Request : TIME
23:01:12,641 DEBUG main:Service:registerRequest:135 - Command : universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a
23:01:12,641 DEBUG main:Service:registerRequest:142 - Requete ajoutée
23:01:12,643 DEBUG main:Service:registerNotification:157 - Notification : HELLO_CLIENT
23:01:12,643 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf
23:01:12,643 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:12,644 DEBUG main:Service:registerNotification:157 - Notification : NEW_CLIENT
23:01:12,644 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f
23:01:12,644 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:12,645 DEBUG main:Service:registerNotification:157 - Notification : CLOSE_CLIENT
23:01:12,646 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c
23:01:12,646 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:12,646 DEBUG main:Server:open:221 - Open server service : true
23:01:12,646 DEBUG Server0:ReaderSocket:<init>:29 - Socket : null
23:01:12,646 DEBUG Server0:Server:run:128 - Reader socket : ReaderSocketStream [socket=null]
23:01:12,646 DEBUG Server0:Server:run:132 - Writer socket : WriterSocketStream [socket=null]
23:01:18,725 DEBUG Server0:Server:run:137 - Client socket : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,727 DEBUG Server0:Server:run:145 - Service socket : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]]]
23:01:18,728 DEBUG Server0:Communicator:<init>:54 - Entité A : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]]]
23:01:18,728 DEBUG Server0:Communicator:<init>:57 - Entité B : Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf]
23:01:18,728 DEBUG Server0:Communicator:<init>:60 - Traducteur : universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2
23:01:18,728 DEBUG Server0:Communicator:<init>:63 - Echanges : 0
23:01:18,728 DEBUG Server0:Communicator:<init>:66 - Thread communication : Thread[Communication0,5,main]
23:01:18,728 DEBUG Server0:Server:run:150 - Communication entre socket et serveur: Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication0,5,main]]
23:01:18,729 DEBUG Thread-0:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,742 DEBUG Server0:Server:run:154 - Communication ouverte : true
23:01:18,742 DEBUG Server0:Server:notifyObserver:178 - Subject notify : HELLO_CLIENT
23:01:18,742 DEBUG Thread-0:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@19761547
23:01:18,742 DEBUG Server0:Server:notifyObserver:179 - Communication : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication0,5,main]]
23:01:18,742 DEBUG Thread-0:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@17b1e819
23:01:18,742 DEBUG Server0:Server:notifyObserver:189 - Notification : universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf
23:01:18,743 DEBUG Server0:Server:notifyObserver:194 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,743 DEBUG Server0:ConverterStringToString:convert:19 - Message to convert : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,743 DEBUG Server0:Communicator:notifyBtoA:181 - Message translate to entity A : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,743 DEBUG Server0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,743 DEBUG Server0:WriterSocketStream:write:31 - Message to write : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,743 DEBUG Server0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@5bc4cd19
23:01:18,743 DEBUG Server0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@55032811
23:01:18,743 DEBUG Server0:Server:run:159 - Notify hello client : true
23:01:18,744 DEBUG Server0:Server:run:163 - Notify new client : true
23:01:18,744 DEBUG Server0:Server:run:169 - Ajout de la communication dans la map
23:01:18,744 DEBUG Server0:ReaderSocket:<init>:29 - Socket : null
23:01:18,744 DEBUG Server0:Server:run:128 - Reader socket : ReaderSocketStream [socket=null]
23:01:18,744 DEBUG Server0:Server:run:132 - Writer socket : WriterSocketStream [socket=null]
23:01:18,744 DEBUG Server0:Server:run:137 - Client socket : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,744 DEBUG Server0:Server:run:145 - Service socket : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]]]
23:01:18,744 DEBUG Server0:Communicator:<init>:54 - Entité A : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]]]
23:01:18,744 DEBUG Server0:Communicator:<init>:57 - Entité B : Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf]
23:01:18,744 DEBUG Server0:Communicator:<init>:60 - Traducteur : universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2
23:01:18,744 DEBUG Server0:Communicator:<init>:63 - Echanges : 0
23:01:18,744 DEBUG Server0:Communicator:<init>:66 - Thread communication : Thread[Communication1,5,main]
23:01:18,744 DEBUG Server0:Server:run:150 - Communication entre socket et serveur: Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication1,5,main]]
23:01:18,749 DEBUG Server0:Server:run:154 - Communication ouverte : true
23:01:18,749 DEBUG Thread-1:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,749 DEBUG Server0:Server:notifyObserver:178 - Subject notify : HELLO_CLIENT
23:01:18,749 DEBUG Thread-1:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@65249852
23:01:18,749 DEBUG Server0:Server:notifyObserver:179 - Communication : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication1,5,main]]
23:01:18,749 DEBUG Server0:Server:notifyObserver:189 - Notification : universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf
23:01:18,749 DEBUG Thread-1:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@6be37a86
23:01:18,750 DEBUG Server0:Server:notifyObserver:194 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,750 DEBUG Server0:ConverterStringToString:convert:19 - Message to convert : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,750 DEBUG Server0:Communicator:notifyBtoA:181 - Message translate to entity A : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,750 DEBUG Server0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,750 DEBUG Server0:WriterSocketStream:write:31 - Message to write : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,750 DEBUG Server0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@513ca669
23:01:18,751 DEBUG Server0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@143e222a
23:01:18,751 DEBUG Server0:Server:run:159 - Notify hello client : true
23:01:18,751 DEBUG Server0:Server:notifyObserver:178 - Subject notify : NEW_CLIENT
23:01:18,751 DEBUG Server0:Server:notifyObserver:179 - Communication : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication0,5,main]]
23:01:18,751 DEBUG Server0:Server:notifyObserver:189 - Notification : universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f
23:01:18,751 DEBUG Server0:Server:notifyObserver:194 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,751 DEBUG Server0:ConverterStringToString:convert:19 - Message to convert : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,751 DEBUG Server0:Communicator:notifyBtoA:181 - Message translate to entity A : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,751 DEBUG Server0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,751 DEBUG Server0:WriterSocketStream:write:31 - Message to write : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,751 DEBUG Server0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@5bc4cd19
23:01:18,751 DEBUG Server0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@696f89b1
23:01:18,751 DEBUG Server0:Server:notifyAllObservers:209 - Notify com : true
23:01:18,751 DEBUG Server0:Server:run:163 - Notify new client : true
23:01:18,751 DEBUG Server0:Server:run:169 - Ajout de la communication dans la map
23:01:18,752 DEBUG Server0:ReaderSocket:<init>:29 - Socket : null
23:01:18,752 DEBUG Server0:Server:run:128 - Reader socket : ReaderSocketStream [socket=null]
23:01:18,752 DEBUG Server0:Server:run:132 - Writer socket : WriterSocketStream [socket=null]
23:01:18,752 DEBUG Server0:Server:run:137 - Client socket : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,752 DEBUG Server0:Server:run:145 - Service socket : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]]]
23:01:18,752 DEBUG Server0:Communicator:<init>:54 - Entité A : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]]]
23:01:18,752 DEBUG Server0:Communicator:<init>:57 - Entité B : Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf]
23:01:18,752 DEBUG Server0:Communicator:<init>:60 - Traducteur : universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2
23:01:18,752 DEBUG Server0:Communicator:<init>:63 - Echanges : 0
23:01:18,752 DEBUG Server0:Communicator:<init>:66 - Thread communication : Thread[Communication2,5,main]
23:01:18,752 DEBUG Server0:Server:run:150 - Communication entre socket et serveur: Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication2,5,main]]
23:01:18,753 DEBUG Thread-2:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,754 DEBUG Server0:Server:run:154 - Communication ouverte : true
23:01:18,762 DEBUG Server0:Server:notifyObserver:178 - Subject notify : HELLO_CLIENT
23:01:18,762 DEBUG Server0:Server:notifyObserver:179 - Communication : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication2,5,main]]
23:01:18,762 DEBUG Server0:Server:notifyObserver:189 - Notification : universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf
23:01:18,762 DEBUG Server0:Server:notifyObserver:194 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,762 DEBUG Server0:ConverterStringToString:convert:19 - Message to convert : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,762 DEBUG Server0:Communicator:notifyBtoA:181 - Message translate to entity A : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,762 DEBUG Server0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,762 DEBUG Server0:WriterSocketStream:write:31 - Message to write : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,760 DEBUG Thread-2:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@398f34ff
23:01:18,763 DEBUG Thread-2:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@3748ddbe
23:01:18,763 DEBUG Server0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@2560772d
23:01:18,763 DEBUG Server0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@11db8206
23:01:18,763 DEBUG Server0:Server:run:159 - Notify hello client : true
23:01:18,763 DEBUG Server0:Server:notifyObserver:178 - Subject notify : NEW_CLIENT
23:01:18,763 DEBUG Server0:Server:notifyObserver:179 - Communication : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication1,5,main]]
23:01:18,764 DEBUG Server0:Server:notifyObserver:189 - Notification : universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f
23:01:18,764 DEBUG Server0:Server:notifyObserver:194 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,764 DEBUG Server0:ConverterStringToString:convert:19 - Message to convert : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,764 DEBUG Server0:Communicator:notifyBtoA:181 - Message translate to entity A : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,764 DEBUG Server0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,764 DEBUG Server0:WriterSocketStream:write:31 - Message to write : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,764 DEBUG Server0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@513ca669
23:01:18,764 DEBUG Server0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@7411dd65
23:01:18,764 DEBUG Server0:Server:notifyAllObservers:209 - Notify com : true
23:01:18,764 DEBUG Server0:Server:notifyObserver:178 - Subject notify : NEW_CLIENT
23:01:18,764 DEBUG Server0:Server:notifyObserver:179 - Communication : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication0,5,main]]
23:01:18,764 DEBUG Server0:Server:notifyObserver:189 - Notification : universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f
23:01:18,764 DEBUG Server0:Server:notifyObserver:194 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,764 DEBUG Server0:ConverterStringToString:convert:19 - Message to convert : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,764 DEBUG Server0:Communicator:notifyBtoA:181 - Message translate to entity A : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,764 DEBUG Server0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,764 DEBUG Server0:WriterSocketStream:write:31 - Message to write : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,764 DEBUG Server0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@5bc4cd19
23:01:18,764 DEBUG Server0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@61a6e057
23:01:18,765 DEBUG Server0:Server:notifyAllObservers:209 - Notify com : true
23:01:18,765 DEBUG Server0:Server:run:163 - Notify new client : true
23:01:18,765 DEBUG Server0:Server:run:169 - Ajout de la communication dans la map
23:01:18,765 DEBUG Server0:ReaderSocket:<init>:29 - Socket : null
23:01:18,765 DEBUG Server0:Server:run:128 - Reader socket : ReaderSocketStream [socket=null]
23:01:18,765 DEBUG Server0:Server:run:132 - Writer socket : WriterSocketStream [socket=null]
23:01:18,779 DEBUG Thread-1:ReaderSocketStream:read:52 - Response : YEAR
23:01:18,783 DEBUG Thread-1:SocketService:run:78 - Message notify : YEAR
23:01:18,783 DEBUG Thread-1:ConverterStringToString:convert:19 - Message to convert : YEAR
23:01:18,783 DEBUG Thread-1:SocketService:run:84 - Message translate notify : YEAR
23:01:18,783 DEBUG Thread-1:Service:isNotify:90 - Message notify : YEAR
23:01:18,783 DEBUG Thread-0:ReaderSocketStream:read:52 - Response : MONTH
23:01:18,784 DEBUG Thread-1:Service:isNotify:95 - Command name notify : YEAR
23:01:18,784 DEBUG Thread-1:Service:isNotify:102 - Is notify : false
23:01:18,784 DEBUG Thread-0:SocketService:run:78 - Message notify : MONTH
23:01:18,784 DEBUG Thread-1:SocketService:run:95 - Message is not a notify
23:01:18,784 DEBUG Thread-0:ConverterStringToString:convert:19 - Message to convert : MONTH
23:01:18,784 DEBUG Thread-0:SocketService:run:84 - Message translate notify : MONTH
23:01:18,784 DEBUG Thread-1:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,784 DEBUG Thread-0:Service:isNotify:90 - Message notify : MONTH
23:01:18,784 DEBUG Thread-1:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@65249852
23:01:18,784 DEBUG Communication1:Communicator:communicateBetweenEntityAAndB:118 - Entité A talk : YEAR
23:01:18,784 DEBUG Thread-0:Service:isNotify:95 - Command name notify : MONTH
23:01:18,784 DEBUG Communication1:ConverterStringToString:convert:19 - Message to convert : YEAR
23:01:18,784 DEBUG Thread-1:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@6fdcf322
23:01:18,784 DEBUG Communication1:Communicator:communicateBetweenEntityAAndB:126 - Message translate to entity B : YEAR
23:01:18,784 DEBUG Thread-0:Service:isNotify:102 - Is notify : false
23:01:18,784 DEBUG Thread-0:SocketService:run:95 - Message is not a notify
23:01:18,784 DEBUG Communication1:ServerService:listen:45 - Response : YEAR
23:01:18,784 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:118 - Entité A talk : MONTH
23:01:18,784 DEBUG Thread-0:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,785 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : MONTH
23:01:18,784 DEBUG Communication1:ServerService:listen:52 - Command name : YEAR
23:01:18,785 DEBUG Communication1:ServerService:listen:65 - Command : universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2
23:01:18,785 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:126 - Message translate to entity B : MONTH
23:01:18,785 DEBUG Communication0:ServerService:listen:45 - Response : MONTH
23:01:18,785 DEBUG Thread-0:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@19761547
23:01:18,785 DEBUG Thread-0:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@138e7e37
23:01:18,796 DEBUG Communication1:Communicator:communicateBetweenEntityAAndB:134 - Entity B understand A : true
23:01:18,796 DEBUG Communication1:Communicator:run:87 - A understand B : true
23:01:18,796 DEBUG Communication1:Communicator:communicateBetweenEntityBAndA:148 - Entité B talk : 59
23:01:18,796 DEBUG Communication0:ServerService:listen:52 - Command name : MONTH
23:01:18,796 DEBUG Communication1:ConverterStringToString:convert:19 - Message to convert : 59
23:01:18,796 DEBUG Communication1:Communicator:communicateBetweenEntityBAndA:156 - Message translate to entity B : 59
23:01:18,796 DEBUG Communication1:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,796 DEBUG Communication1:WriterSocketStream:write:31 - Message to write : 59
23:01:18,796 DEBUG Communication0:ServerService:listen:65 - Command : universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d
23:01:18,796 DEBUG Communication1:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@513ca669
23:01:18,797 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:134 - Entity B understand A : true
23:01:18,797 DEBUG Communication0:Communicator:run:87 - A understand B : true
23:01:18,797 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:148 - Entité B talk : 28
23:01:18,797 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : 28
23:01:18,797 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:156 - Message translate to entity B : 28
23:01:18,797 DEBUG Communication0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,797 DEBUG Communication0:WriterSocketStream:write:31 - Message to write : 28
23:01:18,797 DEBUG Communication0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@5bc4cd19
23:01:18,797 DEBUG Communication0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@72857d83
23:01:18,797 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:164 - Entity A understand B : true
23:01:18,797 DEBUG Communication0:Communicator:run:94 - B understand A : true
23:01:18,797 DEBUG Communication0:Communicator:run:100 - Echanges : 1
23:01:18,797 DEBUG Communication0:Communicator:run:101 - EntityA is open : true
23:01:18,797 DEBUG Communication0:Communicator:run:102 - EntityA is limited : false
23:01:18,797 DEBUG Communication0:Communicator:run:103 - EntityB is open : true
23:01:18,797 DEBUG Communication0:Communicator:run:104 - EntityB is limited : false
23:01:18,798 DEBUG Communication1:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@50a7c1f9
23:01:18,799 DEBUG Communication1:Communicator:communicateBetweenEntityBAndA:164 - Entity A understand B : true
23:01:18,799 DEBUG Communication1:Communicator:run:94 - B understand A : true
23:01:18,799 DEBUG Communication1:Communicator:run:100 - Echanges : 1
23:01:18,799 DEBUG Communication1:Communicator:run:101 - EntityA is open : true
23:01:18,800 DEBUG Communication1:Communicator:run:102 - EntityA is limited : false
23:01:18,800 DEBUG Communication1:Communicator:run:103 - EntityB is open : true
23:01:18,800 DEBUG Communication1:Communicator:run:104 - EntityB is limited : false
23:01:18,800 DEBUG Thread-0:ReaderSocketStream:read:52 - Response : DAY
23:01:18,800 DEBUG Thread-0:SocketService:run:78 - Message notify : DAY
23:01:18,800 DEBUG Thread-0:ConverterStringToString:convert:19 - Message to convert : DAY
23:01:18,800 DEBUG Thread-0:SocketService:run:84 - Message translate notify : DAY
23:01:18,800 DEBUG Thread-0:Service:isNotify:90 - Message notify : DAY
23:01:18,800 DEBUG Thread-0:Service:isNotify:95 - Command name notify : DAY
23:01:18,800 DEBUG Thread-0:Service:isNotify:102 - Is notify : false
23:01:18,801 DEBUG Thread-0:SocketService:run:95 - Message is not a notify
23:01:18,801 DEBUG Thread-0:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,801 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:118 - Entité A talk : DAY
23:01:18,801 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : DAY
23:01:18,801 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:126 - Message translate to entity B : DAY
23:01:18,801 DEBUG Communication0:ServerService:listen:45 - Response : DAY
23:01:18,801 DEBUG Communication0:ServerService:listen:52 - Command name : DAY
23:01:18,801 DEBUG Communication0:ServerService:listen:65 - Command : universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5
23:01:18,801 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:134 - Entity B understand A : true
23:01:18,801 DEBUG Communication0:Communicator:run:87 - A understand B : true
23:01:18,801 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:148 - Entité B talk : 6
23:01:18,801 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : 6
23:01:18,801 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:156 - Message translate to entity B : 6
23:01:18,801 DEBUG Communication0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,801 DEBUG Communication0:WriterSocketStream:write:31 - Message to write : 6
23:01:18,801 DEBUG Communication0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@5bc4cd19
23:01:18,801 DEBUG Communication0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@7c429da3
23:01:18,801 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:164 - Entity A understand B : true
23:01:18,802 DEBUG Communication0:Communicator:run:94 - B understand A : true
23:01:18,802 DEBUG Communication0:Communicator:run:100 - Echanges : 2
23:01:18,802 DEBUG Communication0:Communicator:run:101 - EntityA is open : true
23:01:18,802 DEBUG Communication0:Communicator:run:102 - EntityA is limited : false
23:01:18,802 DEBUG Communication0:Communicator:run:103 - EntityB is open : true
23:01:18,802 DEBUG Communication0:Communicator:run:104 - EntityB is limited : false
23:01:18,801 DEBUG Thread-0:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@19761547
23:01:18,802 DEBUG Thread-0:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@568ce222
23:01:18,803 DEBUG Thread-1:ReaderSocketStream:read:52 - Response : TIME
23:01:18,803 DEBUG Thread-1:SocketService:run:78 - Message notify : TIME
23:01:18,803 DEBUG Thread-1:ConverterStringToString:convert:19 - Message to convert : TIME
23:01:18,803 DEBUG Thread-1:SocketService:run:84 - Message translate notify : TIME
23:01:18,803 DEBUG Thread-1:Service:isNotify:90 - Message notify : TIME
23:01:18,803 DEBUG Thread-1:Service:isNotify:95 - Command name notify : TIME
23:01:18,803 DEBUG Thread-1:Service:isNotify:102 - Is notify : false
23:01:18,803 DEBUG Thread-1:SocketService:run:95 - Message is not a notify
23:01:18,803 DEBUG Thread-1:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,803 DEBUG Thread-1:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@65249852
23:01:18,803 DEBUG Thread-1:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@2ade70bd
23:01:18,811 DEBUG Communication1:Communicator:communicateBetweenEntityAAndB:118 - Entité A talk : TIME
23:01:18,811 DEBUG Communication1:ConverterStringToString:convert:19 - Message to convert : TIME
23:01:18,811 DEBUG Communication1:Communicator:communicateBetweenEntityAAndB:126 - Message translate to entity B : TIME
23:01:18,811 DEBUG Communication1:ServerService:listen:45 - Response : TIME
23:01:18,811 DEBUG Communication1:ServerService:listen:52 - Command name : TIME
23:01:18,811 DEBUG Communication1:ServerService:listen:65 - Command : universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a
23:01:18,811 DEBUG Communication1:Communicator:communicateBetweenEntityAAndB:134 - Entity B understand A : true
23:01:18,811 DEBUG Communication1:Communicator:run:87 - A understand B : true
23:01:18,811 DEBUG Communication1:Communicator:communicateBetweenEntityBAndA:148 - Entité B talk : 23
23:01:18,812 DEBUG Communication1:ConverterStringToString:convert:19 - Message to convert : 23
23:01:18,812 DEBUG Communication1:Communicator:communicateBetweenEntityBAndA:156 - Message translate to entity B : 23
23:01:18,812 DEBUG Communication1:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,812 DEBUG Communication1:WriterSocketStream:write:31 - Message to write : 23
23:01:18,812 DEBUG Communication1:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@513ca669
23:01:18,812 DEBUG Communication1:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@1bdb87ac
23:01:18,814 DEBUG Communication1:Communicator:communicateBetweenEntityBAndA:164 - Entity A understand B : true
23:01:18,814 DEBUG Communication1:Communicator:run:94 - B understand A : true
23:01:18,814 DEBUG Thread-1:ReaderSocketStream:read:52 - Response : CLOSE_CLIENT;35756
23:01:18,814 DEBUG Thread-1:SocketService:run:78 - Message notify : CLOSE_CLIENT;35756
23:01:18,814 DEBUG Thread-1:ConverterStringToString:convert:19 - Message to convert : CLOSE_CLIENT;35756
23:01:18,814 DEBUG Communication1:Communicator:run:100 - Echanges : 2
23:01:18,814 DEBUG Thread-1:SocketService:run:84 - Message translate notify : CLOSE_CLIENT;35756
23:01:18,814 DEBUG Thread-1:Service:isNotify:90 - Message notify : CLOSE_CLIENT;35756
23:01:18,814 DEBUG Communication1:Communicator:run:101 - EntityA is open : true
23:01:18,814 DEBUG Communication1:Communicator:run:102 - EntityA is limited : false
23:01:18,815 DEBUG Thread-1:Service:isNotify:95 - Command name notify : CLOSE_CLIENT
23:01:18,815 DEBUG Thread-1:Service:isNotify:102 - Is notify : true
23:01:18,815 DEBUG Thread-1:SocketService:run:92 - Message is notify
23:01:18,815 DEBUG Communication1:Communicator:run:103 - EntityB is open : true
23:01:18,815 DEBUG Thread-1:Service:notify:109 - Message notify : CLOSE_CLIENT;35756
23:01:18,815 DEBUG Thread-1:Service:notify:113 - Command name notify : CLOSE_CLIENT
23:01:18,815 DEBUG Thread-1:Service:notify:120 - Command : universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c
23:01:18,815 DEBUG Thread-1:CloseClientNotify:send:26 - Port : 35756
23:01:18,815 DEBUG Thread-1:CloseClientNotify:send:30 - Client removed : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=2, threadCommunication=Thread[Communication1,5,main]]
23:01:18,816 DEBUG Thread-1:CloseClientNotify:send:36 - Client stopped : false
23:01:18,816 DEBUG Thread-1:CloseClientNotify:send:40 - Socket service : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35756,localport=2345]]]
23:01:18,816 DEBUG Thread-1:Server:notifyObserver:178 - Subject notify : CLOSE_CLIENT
23:01:18,816 DEBUG Thread-1:Server:notifyObserver:179 - Communication : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=2, threadCommunication=Thread[Communication0,5,main]]
23:01:18,816 DEBUG Thread-1:Server:notifyObserver:189 - Notification : universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c
23:01:18,816 DEBUG Thread-1:Server:notifyObserver:194 - Message notify : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,816 DEBUG Thread-1:ConverterStringToString:convert:19 - Message to convert : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,816 DEBUG Thread-1:Communicator:notifyBtoA:181 - Message translate to entity A : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,816 DEBUG Thread-1:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,816 DEBUG Thread-1:WriterSocketStream:write:31 - Message to write : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,816 DEBUG Thread-1:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@5bc4cd19
23:01:18,816 DEBUG Thread-1:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@6bf99bbc
23:01:18,816 DEBUG Thread-1:Server:notifyAllObservers:209 - Notify com : true
23:01:18,816 DEBUG Thread-1:Server:notifyObserver:178 - Subject notify : CLOSE_CLIENT
23:01:18,816 DEBUG Thread-1:Server:notifyObserver:179 - Communication : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication2,5,main]]
23:01:18,816 DEBUG Thread-1:Server:notifyObserver:189 - Notification : universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c
23:01:18,816 DEBUG Thread-1:Server:notifyObserver:194 - Message notify : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,816 DEBUG Thread-1:ConverterStringToString:convert:19 - Message to convert : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,816 DEBUG Thread-1:Communicator:notifyBtoA:181 - Message translate to entity A : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,817 DEBUG Thread-1:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,817 DEBUG Thread-1:WriterSocketStream:write:31 - Message to write : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,817 DEBUG Thread-1:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@2560772d
23:01:18,817 DEBUG Communication1:Communicator:run:104 - EntityB is limited : false
23:01:18,817 DEBUG Thread-1:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@1bbefb0f
23:01:18,818 DEBUG Thread-1:Server:notifyAllObservers:209 - Notify com : true
23:01:18,818 DEBUG Thread-1:CloseClientNotify:send:45 - Notify close : true
23:01:19,821 DEBUG Thread-0:ReaderSocketStream:read:52 - Response : CLOSE_CLIENT;35754
23:01:19,822 DEBUG Thread-0:SocketService:run:78 - Message notify : CLOSE_CLIENT;35754
23:01:19,822 DEBUG Thread-0:ConverterStringToString:convert:19 - Message to convert : CLOSE_CLIENT;35754
23:01:19,822 DEBUG Thread-0:SocketService:run:84 - Message translate notify : CLOSE_CLIENT;35754
23:01:19,822 DEBUG Thread-0:Service:isNotify:90 - Message notify : CLOSE_CLIENT;35754
23:01:19,822 DEBUG Thread-0:Service:isNotify:95 - Command name notify : CLOSE_CLIENT
23:01:19,822 DEBUG Thread-0:Service:isNotify:102 - Is notify : true
23:01:19,822 DEBUG Thread-0:SocketService:run:92 - Message is notify
23:01:19,823 DEBUG Thread-0:Service:notify:109 - Message notify : CLOSE_CLIENT;35754
23:01:19,823 DEBUG Thread-0:Service:notify:113 - Command name notify : CLOSE_CLIENT
23:01:19,823 DEBUG Thread-0:Service:notify:120 - Command : universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c
23:01:19,823 DEBUG Thread-0:CloseClientNotify:send:26 - Port : 35754
23:01:19,824 DEBUG Thread-0:CloseClientNotify:send:30 - Client removed : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=2, threadCommunication=Thread[Communication0,5,main]]
23:01:19,824 DEBUG Thread-0:CloseClientNotify:send:36 - Client stopped : false
23:01:19,824 DEBUG Thread-0:CloseClientNotify:send:40 - Socket service : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35754,localport=2345]]]
23:01:19,824 DEBUG Thread-0:Server:notifyObserver:178 - Subject notify : CLOSE_CLIENT
23:01:19,825 DEBUG Thread-0:Server:notifyObserver:179 - Communication : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication2,5,main]]
23:01:19,825 DEBUG Thread-0:Server:notifyObserver:189 - Notification : universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c
23:01:19,825 DEBUG Thread-0:Server:notifyObserver:194 - Message notify : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:19,825 DEBUG Thread-0:ConverterStringToString:convert:19 - Message to convert : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:19,825 DEBUG Thread-0:Communicator:notifyBtoA:181 - Message translate to entity A : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:19,825 DEBUG Thread-0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:19,826 DEBUG Thread-0:WriterSocketStream:write:31 - Message to write : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:19,826 DEBUG Thread-0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@2560772d
23:01:19,826 DEBUG Thread-0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@7ff90999
23:01:19,826 DEBUG Thread-0:Server:notifyAllObservers:209 - Notify com : true
23:01:19,827 DEBUG Thread-0:CloseClientNotify:send:45 - Notify close : true
23:01:19,832 DEBUG Thread-2:ReaderSocketStream:read:52 - Response : CLOSE_CLIENT;35758
23:01:19,832 DEBUG Thread-2:SocketService:run:78 - Message notify : CLOSE_CLIENT;35758
23:01:19,832 DEBUG Thread-2:ConverterStringToString:convert:19 - Message to convert : CLOSE_CLIENT;35758
23:01:19,832 DEBUG Thread-2:SocketService:run:84 - Message translate notify : CLOSE_CLIENT;35758
23:01:19,833 DEBUG Thread-2:Service:isNotify:90 - Message notify : CLOSE_CLIENT;35758
23:01:19,833 DEBUG Thread-2:Service:isNotify:95 - Command name notify : CLOSE_CLIENT
23:01:19,833 DEBUG Thread-2:Service:isNotify:102 - Is notify : true
23:01:19,833 DEBUG Thread-2:SocketService:run:92 - Message is notify
23:01:19,833 DEBUG Thread-2:Service:notify:109 - Message notify : CLOSE_CLIENT;35758
23:01:19,833 DEBUG Thread-2:Service:notify:113 - Command name notify : CLOSE_CLIENT
23:01:19,834 DEBUG Thread-2:Service:notify:120 - Command : universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c
23:01:19,834 DEBUG Thread-2:CloseClientNotify:send:26 - Port : 35758
23:01:19,834 DEBUG Thread-2:CloseClientNotify:send:30 - Client removed : Communicator [entityA=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]]], entityB=Service [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.server.command.request.DateUtilMonthRequest@cc34f4d, YEAR=universite.angers.master.info.network.server.command.request.DateUtilYearRequest@17a7cec2, TIME=universite.angers.master.info.network.server.command.request.DateUtilHourRequest@65b3120a, DAY=universite.angers.master.info.network.server.command.request.DateUtilDayRequest@4edde6e5}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.server.command.notify.HelloClientNotify@6f539caf, NEW_CLIENT=universite.angers.master.info.network.server.command.notify.NewClientNotify@79fc0f2f, CLOSE_CLIENT=universite.angers.master.info.network.server.command.notify.CloseClientNotify@50040f0c}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.server.command.ConverterCommandNameServer@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], translator=universite.angers.master.info.api.translater.TranslaterStringToString@1b2c6ec2, request=0, threadCommunication=Thread[Communication2,5,main]]
23:01:19,835 DEBUG Thread-2:CloseClientNotify:send:36 - Client stopped : false
23:01:19,835 DEBUG Thread-2:CloseClientNotify:send:40 - Socket service : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=35758,localport=2345]]]
23:01:19,835 DEBUG Thread-2:CloseClientNotify:send:45 - Notify close : true
	 */
