package universite.angers.master.info.network.server.command;

import universite.angers.master.info.api.converter.Convertable;

public class ConverterCommandNameServer implements Convertable<String, String> {

	@Override
	public String convert(String message) {
		return message.split(";")[0];
	}
}
