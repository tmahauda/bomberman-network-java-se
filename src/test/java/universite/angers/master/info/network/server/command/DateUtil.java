package universite.angers.master.info.network.server.command;

import java.util.Calendar;
import java.util.Date;

/**
 * La classe receveur
 * @author etudiant
 *
 */
public class DateUtil {

	private Date date;
	
	public DateUtil() {
		this.date = new Date();
	}
	
	/**
	 * @return the day
	 */
	public String getDay() {
		Calendar c = Calendar.getInstance();
		c.setTime(this.date);
		return String.valueOf(c.get(Calendar.DAY_OF_WEEK));
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		Calendar c = Calendar.getInstance();
		c.setTime(this.date);
		return String.valueOf(c.get(Calendar.DAY_OF_MONTH));
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		Calendar c = Calendar.getInstance();
		c.setTime(this.date);
		return String.valueOf(c.get(Calendar.DAY_OF_YEAR));
	}
	
	/**
	 * @return the hour
	 */
	public String getHour() {
		Calendar c = Calendar.getInstance();
		c.setTime(this.date);
		return String.valueOf(c.get(Calendar.HOUR_OF_DAY));
	}
}
