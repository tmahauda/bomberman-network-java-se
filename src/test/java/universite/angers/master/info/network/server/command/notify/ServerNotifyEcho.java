package universite.angers.master.info.network.server.command.notify;

import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.network.service.Commandable;

public class ServerNotifyEcho implements Commandable<String> {

	@Override
	public boolean send(String message) {
		return true;
	}

	@Override
	public String receive(Object arg) {
		return Subject.NOTIFY_ECHO.getName()+";Echo client";
	}
}
