package universite.angers.master.info.network.server.command.notify;

import java.net.Socket;
import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.network.service.Commandable;

public class ServerNotifyHello implements Commandable<String> {

	@Override
	public boolean send(String message) {
		return true;
	}

	@Override
	public String receive(Object arg) {
		Socket client = (Socket)arg;
		
		return Subject.NOTIFY_HELLO.getName()+";Welcome client : " + client;
	}
}
