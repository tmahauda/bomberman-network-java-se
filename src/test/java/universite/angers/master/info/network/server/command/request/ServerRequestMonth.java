package universite.angers.master.info.network.server.command.request;

import universite.angers.master.info.network.server.command.DateUtil;
import universite.angers.master.info.network.service.Commandable;

public class ServerRequestMonth implements Commandable<String> {

	private DateUtil date;
	
	public ServerRequestMonth(DateUtil date) {
		this.date = date;
	}
	
	@Override
	public boolean send(String message) {
		return true;
	}

	@Override
	public String receive(Object arg) {
		return this.date.getMonth();
	}
}
