package universite.angers.master.info.network.server.model;

import java.util.Date;
import java.util.UUID;

public class Game {

	private String id;
	private int score;
	private int level;
	private Date date;
	private boolean lose;

	/**
	 * Constructeur par défaut sans argument pour l'ORM
	 */
	public Game() {
		this(0, 0, new Date(), false);
	}
	
	/**
	 * Constructeur avec arguments
	 * @param score
	 * @param level
	 * @param date
	 * @param lose
	 */
	public Game(int score, int level, Date date, boolean lose) {
		this.id = UUID.randomUUID().toString();
		this.score = score;
		this.level = level;
		this.date = date;
		this.lose = lose;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the lose
	 */
	public boolean isLose() {
		return lose;
	}

	/**
	 * @param lose the lose to set
	 */
	public void setLose(boolean lose) {
		this.lose = lose;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + level;
		result = prime * result + (lose ? 1231 : 1237);
		result = prime * result + score;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (level != other.level)
			return false;
		if (lose != other.lose)
			return false;
		if (score != other.score)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Game [id=" + id + ", score=" + score + ", level=" + level + ", date=" + date + ", lose=" + lose + "]";
	}
}
